1.11.0
- 2d / 3d api
- PixelMap does not have direct access. You can use DirectAccessPixelMap
- Optimize

1.10.0

- SimpleMaterial - no any shading
- PhongMaterial - phong shading
- Significantly improved graphics builder

1.9.1

- Can draw round-rect

1.9.0

- Vector Graph supports complex objects now casperix A minute ago

1.8.0

- use package `ru.casperix.renderer`
- back to java 17

1.7.8

- auto deploy