# Render2d Api

Fixed API for 2d-renderer engine. It is usually used for the internal needs of other libraries

### Renderer

```
val renderer:Renderer2D = ...
val graphic:VectorGraphic = ...

renderer.drawGraphic(graphic)
```
### Vector graphic

You can create
[VectorGraphic](src%2FcommonMain%2Fkotlin%2Fru%2Fcasperix%2Frenderer%2Fvector%2FVectorGraphic.kt)
on the fly, painting it in arbitrary materials
```
val graphic = VectorGraphicBuilder.build {
    add(Color.RED) {
        addCircle(Vector2f.ZERO, 4f, 3f)
    }

    addCircle(Color.GREEN, Vector2f.ZERO, 3f)
    addCircle(SimpleMaterial(Color.BLUE), Vector2f.ONE, 2f)
}
```

### Materials

```
val simple = SimpleMaterial(Color.BLUE)
val phong = PhongMaterial(ConstantColorSource(Color.BLUE))
val pbr = PBRMaterial(ConstantColorSource(Color.BLUE))
```

### Lights
- [DirectionLight.kt](src%2FcommonMain%2Fkotlin%2Fru%2Fcasperix%2Frenderer%2Flight%2FDirectionLight.kt)
- [PointLight.kt](src%2FcommonMain%2Fkotlin%2Fru%2Fcasperix%2Frenderer%2Flight%2FPointLight.kt)

You can also specify attenuation and spot.


### Pixel maps

[Rgba8PixelMap.kt](src%2FcommonMain%2Fkotlin%2Fru%2Fcasperix%2Frenderer%2Fpixel_map%2FRgba8PixelMap.kt)
[Rgb8PixelMap.kt](src%2FcommonMain%2Fkotlin%2Fru%2Fcasperix%2Frenderer%2Fpixel_map%2FRgb8PixelMap.kt)
[Rgba16PixelMap.kt](src%2FcommonMain%2Fkotlin%2Fru%2Fcasperix%2Frenderer%2Fpixel_map%2FRgba16PixelMap.kt)
[Rgb32PixelMap.kt](src%2FcommonMain%2Fkotlin%2Fru%2Fcasperix%2Frenderer%2Fpixel_map%2FRgb32PixelMap.kt)
[Rgba32PixelMap.kt](src%2FcommonMain%2Fkotlin%2Fru%2Fcasperix%2Frenderer%2Fpixel_map%2FRgba32PixelMap.kt)
[Rgb16PixelMap.kt](src%2FcommonMain%2Fkotlin%2Fru%2Fcasperix%2Frenderer%2Fpixel_map%2FRgb16PixelMap.kt)
[Int8PixelMap.kt](src%2FcommonMain%2Fkotlin%2Fru%2Fcasperix%2Frenderer%2Fpixel_map%2FInt8PixelMap.kt)
[Float32PixelMap.kt](src%2FcommonMain%2Fkotlin%2Fru%2Fcasperix%2Frenderer%2Fpixel_map%2FFloat32PixelMap.kt)




## deploy

### Local
Just add water:
- `./gradlew publishToMavenLocal`

### Nexus
Need variables (ask the administrator):
`ossrhUsername`,
`ossrhPassowrd`,
`signing.keyId`,
`signing.password`,
`signing.secretKeyRingFile`.


Publishing, closing, releasing:
- `./gradlew publishToSonatype`
- `./gradlew findSonatypeStagingRepository closeAndReleaseSonatypeStagingRepository`

In case of troubles, see also:
- https://s01.oss.sonatype.org/#stagingRepositories
- https://github.com/gradle-nexus/publish-plugin