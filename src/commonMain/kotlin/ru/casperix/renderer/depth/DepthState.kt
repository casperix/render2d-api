package ru.casperix.renderer.depth


enum class DepthState {
    DISABLE,
    READABLE,
    WRITABLE,
}