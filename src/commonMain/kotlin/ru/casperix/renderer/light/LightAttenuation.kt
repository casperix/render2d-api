package ru.casperix.renderer.light

data class LightAttenuation(val constant: Float, val linear: Float, val quadratic: Float)