package ru.casperix.renderer.light

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.vector.float32.Vector3f

data class LightSpot(
    val direction: Vector3f,
    val angularSize: DegreeFloat = DegreeFloat(30f),
    val angularBorderSize: DegreeFloat = DegreeFloat(0f),
)