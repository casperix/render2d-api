package ru.casperix.renderer.light

import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.vector.float32.Vector3f

data class PointLight(
    val position: Vector3f,
    val colors: LightColors,
    val attenuation: LightAttenuation,
    val spot: LightSpot? = null,
) : Light {
    companion object {
        val DEFAULT = PointLight(Vector3f.ZERO, LightColors(RgbColor3f(0.2f), RgbColor3f(0.5f), RgbColor3f(1f)), LightAttenuation(1f, 0.09f, 0.032f))
    }
}


