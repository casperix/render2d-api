package ru.casperix.renderer.light

import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.vector.float32.Vector3f

data class DirectionLight(
    val direction: Vector3f,
    val colors: LightColors,
) : Light {
    companion object {
        val DEFAULT = DirectionLight(Vector3f(1f, 1f, -1f).normalize(), LightColors(RgbColor3f(0.2f), RgbColor3f(0.5f), RgbColor3f(1f)))
    }
}