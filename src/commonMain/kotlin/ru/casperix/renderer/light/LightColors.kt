package ru.casperix.renderer.light

import ru.casperix.math.color.rgb.RgbColor3f

data class LightColors(
    val ambientColor: RgbColor3f,
    val diffuseColor: RgbColor3f,
    val specularColor: RgbColor3f,
)