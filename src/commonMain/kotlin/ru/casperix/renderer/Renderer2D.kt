package ru.casperix.renderer

import ru.casperix.math.axis_aligned.float32.Dimension2f
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.renderer.misc.orthographic
import ru.casperix.renderer.vector.VectorGraphic2D
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder2D

interface Renderer2D : Renderer {
    var viewMatrix: Matrix3f
    var projectionMatrix: Matrix3f

    fun draw(graphic: VectorGraphic2D, transform: Matrix3f = Matrix3f.IDENTITY)

    fun draw(transform: Matrix3f = Matrix3f.IDENTITY, graphicBuilder: (VectorGraphicBuilder2D.() -> Unit)) {
        draw(VectorGraphicBuilder2D.build(graphicBuilder), transform)
    }

    fun setOrthographicCamera(dimension: Dimension2f, centered:Boolean= true, yDown:Boolean = true) {
        viewPort = dimension.roundToDimension2i()
        projectionMatrix = Matrix3f.orthographic(dimension , centered, yDown)
    }
}