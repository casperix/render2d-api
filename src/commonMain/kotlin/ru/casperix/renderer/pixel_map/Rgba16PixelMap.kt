package ru.casperix.renderer.pixel_map

import ru.casperix.math.array.uint8.UByteArray3D
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.color.rgba.RgbaColor4s
import ru.casperix.renderer.pixel_map.codec.H_RGBACodec

class Rgba16PixelMap(byteArray: UByteArray3D, override val name: String = "") : DirectAccessPixelMap<RgbaColor4s, H_RGBACodec>(byteArray, H_RGBACodec) {
    constructor(width: Int, height: Int, name: String = "") : this(UByteArray3D(width, height, H_RGBACodec.bytesPerPixel), name)
    constructor(dimension: Dimension2i, name: String = "") : this(dimension.width, dimension.height, name)

    fun multiplyByAlpha() {
        (0 until width).forEach { x ->
            (0 until height).forEach { y ->
                val color = get(x, y)
                set(
                    x, y,
                    RgbaColor4s(
                        (color.red * color.alpha / 65536u).toUShort(),
                        (color.green * color.alpha / 65536u).toUShort(),
                        (color.blue * color.alpha / 65536u).toUShort(),
                        color.alpha
                    )
                )
            }
        }
    }
}