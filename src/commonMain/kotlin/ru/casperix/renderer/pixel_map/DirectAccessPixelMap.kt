package ru.casperix.renderer.pixel_map

import ru.casperix.math.array.MutableMap2D
import ru.casperix.math.array.uint8.UByteArray3D
import ru.casperix.math.array.uint8.UByteBasedMap2D
import ru.casperix.renderer.Resource
import ru.casperix.renderer.pixel_map.codec.PixelCodec

abstract class DirectAccessPixelMap<ColorType : Any, Codec : PixelCodec<ColorType>>(
    val byteArray: UByteArray3D,
    val pixelCodec: Codec
) : PixelMap, MutableMap2D<ColorType> by UByteBasedMap2D(byteArray, pixelCodec), Resource() {

    override val bytesPerPixels get() = pixelCodec.bytesPerPixel

    init {
        if (pixelCodec.bytesPerPixel != byteArray.dimension.z) {
            throw Exception("Codec ${pixelCodec::class.simpleName} expected ${pixelCodec.bytesPerPixel} bpp. But actual ${byteArray.dimension.z} bpp")
        }
    }

}