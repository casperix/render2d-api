package ru.casperix.renderer.pixel_map

import ru.casperix.math.array.MutableMap2D
import ru.casperix.math.color.Color
import ru.casperix.math.vector.int32.Vector2i

/**
 * Allows you to use any color format for drawing on any map (that supports this interface)
 */
interface ColorMap<ColorType : Any> : MutableMap2D<ColorType> {
    fun convertColor(color: Color): ColorType

    fun set(x: Int, y: Int, color: Color) = set(x, y, convertColor(color))
    fun set(position: Vector2i, color: Color) = set(position, convertColor(color))
}