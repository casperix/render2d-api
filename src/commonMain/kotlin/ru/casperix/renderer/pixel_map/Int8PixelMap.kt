package ru.casperix.renderer.pixel_map

import ru.casperix.math.array.uint8.UByteArray3D
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.renderer.pixel_map.codec.SingleByteCodec

class Int8PixelMap(byteArray: UByteArray3D, override val name: String = "") : DirectAccessPixelMap<UByte, SingleByteCodec>(byteArray, SingleByteCodec) {
    constructor(width: Int, height: Int, name: String = "") : this(UByteArray3D(width, height, SingleByteCodec.bytesPerPixel), name)
    constructor(dimension: Dimension2i, name: String = "") : this(dimension.width, dimension.height, name)
}