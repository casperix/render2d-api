package ru.casperix.renderer.pixel_map

import ru.casperix.math.array.uint8.UByteArray3D
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.renderer.pixel_map.codec.RGBFloatCodec

class Rgb32PixelMap(byteArray: UByteArray3D, override val name: String = "") : DirectAccessPixelMap<RgbColor3f, RGBFloatCodec>(byteArray, RGBFloatCodec),
    ColorMap<RgbColor3f> {
    constructor(width: Int, height: Int, name: String = "") : this(UByteArray3D(width, height, RGBFloatCodec.bytesPerPixel), name)
    constructor(dimension: Dimension2i, name: String = "") : this(dimension.width, dimension.height, name)

    override fun convertColor(color: Color) = color.toRGB().toColor3f()
}