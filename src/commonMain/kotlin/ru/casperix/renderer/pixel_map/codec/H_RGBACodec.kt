package ru.casperix.renderer.pixel_map.codec

import ru.casperix.math.color.rgba.RgbaColor4s


/**
 * TODO: test it
 */
object H_RGBACodec : PixelCodec<RgbaColor4s> {
    override val componentsPerPixel = 4
    override val bytesPerComponent = 2

    override fun decode(bytes: UByteArray): RgbaColor4s {
        return RgbaColor4s(
            (bytes[0] * 255u + bytes[1]).toUShort(),
            (bytes[2] * 255u + bytes[3]).toUShort(),
            (bytes[4] * 255u + bytes[5]).toUShort(),
            (bytes[6] * 255u + bytes[7]).toUShort(),
        )
    }

    override fun encode(custom: RgbaColor4s): UByteArray {
        return UByteArray(bytesPerPixel).apply {
            this[0] = (custom.red / 255u).toUByte()
            this[1] = custom.red.toUByte()
            this[2] = (custom.green / 255u).toUByte()
            this[3] = custom.green.toUByte()
            this[4] = (custom.blue / 255u).toUByte()
            this[5] = custom.blue.toUByte()
            this[6] = (custom.alpha / 255u).toUByte()
            this[7] = custom.alpha.toUByte()
        }
    }
}

