package ru.casperix.renderer.pixel_map.codec

import ru.casperix.math.array.int8.toBytes
import ru.casperix.math.color.rgba.RgbaColor4f
import ru.casperix.renderer.misc.fromBytesWithOffset


object RGBAFloatCodec : PixelCodec<RgbaColor4f> {
    override val componentsPerPixel = 4
    override val bytesPerComponent = 4

    override fun decode(bytes: UByteArray) = RgbaColor4f(
        Float.fromBytesWithOffset(bytes, 0),
        Float.fromBytesWithOffset(bytes, 4),
        Float.fromBytesWithOffset(bytes, 8),
        Float.fromBytesWithOffset(bytes, 12),
    )

    override fun encode(custom: RgbaColor4f): UByteArray {
        return (custom.red.toBytes() + custom.green.toBytes() + custom.blue.toBytes() + custom.alpha.toBytes()).asUByteArray()
    }
}

