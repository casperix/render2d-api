package ru.casperix.renderer.pixel_map.codec

import ru.casperix.math.array.int8.toBytes
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.renderer.misc.fromBytesWithOffset


object RGBFloatCodec : PixelCodec<RgbColor3f> {
    override val componentsPerPixel = 3
    override val bytesPerComponent = 4

    override fun decode(bytes: UByteArray) = RgbColor3f(
        Float.fromBytesWithOffset(bytes, 0),
        Float.fromBytesWithOffset(bytes, 4),
        Float.fromBytesWithOffset(bytes, 8),
    )

    override fun encode(custom: RgbColor3f): UByteArray {
        return (custom.red.toBytes() + custom.green.toBytes() + custom.blue.toBytes()).asUByteArray()
    }
}

