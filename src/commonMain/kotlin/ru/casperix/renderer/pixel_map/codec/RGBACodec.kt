package ru.casperix.renderer.pixel_map.codec

import ru.casperix.math.color.rgba.RgbaColor4b

object RGBACodec : PixelCodec<RgbaColor4b> {
    override val componentsPerPixel = 4
    override val bytesPerComponent = 1

    override fun decode(bytes: UByteArray): RgbaColor4b {
        return RgbaColor4b(bytes[0], bytes[1], bytes[2], bytes[3])
    }

    override fun encode(custom: RgbaColor4b): UByteArray {
        return UByteArray(bytesPerPixel).apply {
            this[0] = custom.red
            this[1] = custom.green
            this[2] = custom.blue
            this[3] = custom.alpha
        }
    }
}
