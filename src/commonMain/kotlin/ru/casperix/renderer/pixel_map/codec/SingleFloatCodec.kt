package ru.casperix.renderer.pixel_map.codec

import ru.casperix.math.array.int8.fromBytes
import ru.casperix.math.array.int8.toBytes

object SingleFloatCodec : PixelCodec<Float> {
    override val componentsPerPixel = 1
    override val bytesPerComponent = 4

    override fun decode(bytes: UByteArray): Float {
        return Float.fromBytes(bytes.asByteArray())
    }

    override fun encode(custom: Float): UByteArray {
        return custom.toBytes().asUByteArray()
    }
}
