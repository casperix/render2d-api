package ru.casperix.renderer.pixel_map.codec

import ru.casperix.math.array.uint8.UByteCodec

interface PixelCodec<Custom> : UByteCodec<Custom> {
    val componentsPerPixel: Int
    val bytesPerComponent: Int
    val bytesPerPixel get() = bytesPerComponent * componentsPerPixel
}