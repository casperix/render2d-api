package ru.casperix.renderer.pixel_map.codec

import ru.casperix.math.color.rgb.RgbColor3s

/**
 * TODO: test it
 */
object H_RGBCodec : PixelCodec<RgbColor3s> {
    override val componentsPerPixel = 3
    override val bytesPerComponent = 2

    override fun decode(bytes: UByteArray): RgbColor3s {
        return RgbColor3s(
            (bytes[0] * 255u + bytes[1]).toUShort(),
            (bytes[2] * 255u + bytes[3]).toUShort(),
            (bytes[4] * 255u + bytes[5]).toUShort(),
        )
    }

    override fun encode(custom: RgbColor3s): UByteArray {
        return UByteArray(bytesPerPixel).apply {
            this[0] = (custom.red / 255u).toUByte()
            this[1] = custom.red.toUByte()
            this[2] = (custom.green / 255u).toUByte()
            this[3] = custom.green.toUByte()
            this[4] = (custom.blue / 255u).toUByte()
            this[5] = custom.blue.toUByte()
        }
    }
}

