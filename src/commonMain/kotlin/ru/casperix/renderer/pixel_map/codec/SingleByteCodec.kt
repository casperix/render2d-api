package ru.casperix.renderer.pixel_map.codec

object SingleByteCodec : PixelCodec<UByte> {
    override val componentsPerPixel = 1
    override val bytesPerComponent = 1

    override fun decode(bytes: UByteArray): UByte {
        return bytes[0]
    }

    override fun encode(custom: UByte): UByteArray {
        return ubyteArrayOf(custom)
    }
}
