package ru.casperix.renderer.pixel_map.codec

import ru.casperix.math.color.rgb.RgbColor3b


object RGBCodec : PixelCodec<RgbColor3b> {
    override val componentsPerPixel = 3
    override val bytesPerComponent = 1

    override fun decode(bytes: UByteArray): RgbColor3b {
        return RgbColor3b(bytes[0], bytes[1], bytes[2])
    }

    override fun encode(custom: RgbColor3b): UByteArray {
        return UByteArray(bytesPerPixel).apply {
            this[0] = custom.red
            this[1] = custom.green
            this[2] = custom.blue
        }
    }
}

