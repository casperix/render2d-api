package ru.casperix.renderer.pixel_map

import ru.casperix.math.vector.int32.Vector2i

/**
 * The pixel map is obvious.
 * If you want direct access to pixels, see [DirectAccessPixelMap]
 */
interface PixelMap {
    val name: String
    val dimension: Vector2i
    val bytesPerPixels: Int
    val width get() = dimension.x
    val height get() = dimension.y
}

