package ru.casperix.renderer.pixel_map

import ru.casperix.math.array.uint8.UByteArray3D
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.renderer.pixel_map.codec.SingleFloatCodec

class Float32PixelMap(byteArray: UByteArray3D, override val name: String = "") : DirectAccessPixelMap<Float, SingleFloatCodec>(byteArray, SingleFloatCodec) {
    constructor(width: Int, height: Int, name: String = "") : this(UByteArray3D(width, height, SingleFloatCodec.bytesPerPixel), name)
    constructor(dimension: Dimension2i, name: String = "") : this(dimension.width, dimension.height, name)
}