package ru.casperix.renderer.pixel_map

import ru.casperix.math.array.uint8.UByteArray3D
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgba.RgbaColor4b
import ru.casperix.renderer.pixel_map.codec.RGBACodec

class Rgba8PixelMap(byteArray: UByteArray3D, override val name: String = "") : DirectAccessPixelMap<RgbaColor4b, RGBACodec>(byteArray, RGBACodec),
    ColorMap<RgbaColor4b> {
    constructor(width: Int, height: Int, name: String = "") : this(UByteArray3D(width, height, RGBACodec.bytesPerPixel), name)
    constructor(dimension: Dimension2i, name: String = "") : this(dimension.width, dimension.height, name)

    override fun convertColor(color: Color) = color.toRGBA().toColor4b()

    fun multiplyByAlpha() {
        (0 until width).forEach { x ->
            (0 until height).forEach { y ->
                val color = get(x, y)
                set(
                    x, y,
                    RgbaColor4b(
                        (color.red * color.alpha / 256u).toUByte(),
                        (color.green * color.alpha / 256u).toUByte(),
                        (color.blue * color.alpha / 256u).toUByte(),
                        color.alpha
                    )
                )
            }
        }
    }
}