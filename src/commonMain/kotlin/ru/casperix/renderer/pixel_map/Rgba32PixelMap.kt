package ru.casperix.renderer.pixel_map

import ru.casperix.math.array.uint8.UByteArray3D
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgba.RgbaColor4f
import ru.casperix.renderer.pixel_map.codec.RGBAFloatCodec

class Rgba32PixelMap(byteArray: UByteArray3D, override val name: String = "") : DirectAccessPixelMap<RgbaColor4f, RGBAFloatCodec>(byteArray, RGBAFloatCodec), ColorMap<RgbaColor4f> {
    constructor(width: Int, height: Int, name: String = "") : this(UByteArray3D(width, height, RGBAFloatCodec.bytesPerPixel), name)
    constructor(dimension: Dimension2i, name: String = "") : this(dimension.width, dimension.height, name)

    override fun convertColor(color: Color) = color.toRGBA().toColor4f()
}