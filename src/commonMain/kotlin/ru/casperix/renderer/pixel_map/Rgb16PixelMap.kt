package ru.casperix.renderer.pixel_map

import ru.casperix.math.array.uint8.UByteArray3D
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.color.rgb.RgbColor3s
import ru.casperix.renderer.pixel_map.codec.H_RGBCodec

class Rgb16PixelMap(byteArray: UByteArray3D, override val name: String = "") : DirectAccessPixelMap<RgbColor3s, H_RGBCodec>(byteArray, H_RGBCodec) {
    constructor(width: Int, height: Int, name: String = "") : this(UByteArray3D(width, height, H_RGBCodec.bytesPerPixel), name)
    constructor(dimension: Dimension2i, name: String = "") : this(dimension.width, dimension.height, name)
}