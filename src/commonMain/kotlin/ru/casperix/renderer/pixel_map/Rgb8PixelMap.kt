package ru.casperix.renderer.pixel_map

import ru.casperix.math.array.uint8.UByteArray3D
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgb.RgbColor3b
import ru.casperix.renderer.pixel_map.codec.RGBCodec

class Rgb8PixelMap(byteArray: UByteArray3D, override val name: String = "") : DirectAccessPixelMap<RgbColor3b, RGBCodec>(byteArray, RGBCodec),
    ColorMap<RgbColor3b> {
    constructor(width: Int, height: Int, name: String = "") : this(UByteArray3D(width, height, RGBCodec.bytesPerPixel), name)
    constructor(dimension: Dimension2i, name: String = "") : this(dimension.width, dimension.height, name)

    override fun convertColor(color: Color) = color.toRGB().toColor3b()
}