package ru.casperix.renderer.material

import ru.casperix.renderer.vector.vertex.ColorFormat
import ru.casperix.renderer.vector.vertex.VectorFormat
import ru.casperix.renderer.vector.vertex.VertexAttributes

interface Material {
    fun createVertexAttributes(positionFormat: VectorFormat, vertexColorFormat: ColorFormat?, hasVertexOpacity:Boolean):VertexAttributes
}
