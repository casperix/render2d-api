package ru.casperix.renderer.material

import ru.casperix.math.color.Color
import ru.casperix.math.color.rgb.RgbColor
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.renderer.vector.vertex.ColorFormat
import ru.casperix.renderer.vector.vertex.VectorFormat
import ru.casperix.renderer.vector.vertex.VertexAttributes

/**
 * Material uses color without change
 */
data class SimpleMaterial(val colorSource: ColorSource, val opacitySource: Vector1Source) : Material {
    constructor(color:Color) : this(color.toRGB(), color.toAlpha())
    constructor(color:RgbColor, opacity:Float = 1f) : this(ConstantColorSource(color.toColor3f()), ConstantVector1Source(opacity))
    constructor(texture: Texture2D) : this(TextureColorSource(texture), TextureVector1Source(texture, 3))
    constructor(pixelMap: PixelMap) : this(Texture2D(pixelMap))

    override fun createVertexAttributes(positionFormat: VectorFormat, vertexColorFormat: ColorFormat?, hasVertexOpacity:Boolean): VertexAttributes {
        val useTexCoord = colorSource is TextureColorSource || opacitySource is TextureVector1Source
        val textureFormat = if (useTexCoord) VectorFormat.VECTOR_2D else null

        return VertexAttributes(positionFormat, textureFormat, null, null, vertexColorFormat, hasVertexOpacity)
    }


}

