package ru.casperix.renderer.material

import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.renderer.vector.vertex.ColorFormat
import ru.casperix.renderer.vector.vertex.VectorFormat
import ru.casperix.renderer.vector.vertex.VertexAttributes

/**
 * Material uses [PBR shading](https://learnopengl.com/PBR/Lighting)
 */
data class PBRMaterial(
    val albedoSource: ColorSource? = null,
    val metallicSource: Vector1Source? = null,
    val roughnessSource: Vector1Source? = null,
    val aoSource: Vector1Source? = null,
    val normalSource: Vector3Source? = null,
    val opacitySource: Vector1Source? = null,
) : Material {
    constructor(
        albedoMap: Texture2D? = null,
        metallicMap: Texture2D? = null,
        roughnessMap: Texture2D? = null,
        aoMap: Texture2D? = null,
        normalMap: Texture2D? = null,
        opacityMap: Texture2D? = null
    ) : this(
        albedoMap?.run { TextureColorSource(this) },
        metallicMap?.run { TextureVector1Source(this) },
        roughnessMap?.run { TextureVector1Source(this) },
        aoMap?.run { TextureVector1Source(this) },
        normalMap?.run { TextureVector3Source(this) },
        opacityMap?.run { TextureVector1Source(this, 3) },
    )

    constructor(
        albedoMap: PixelMap? = null,
        metallicMap: PixelMap? = null,
        roughnessMap: PixelMap? = null,
        aoMap: PixelMap? = null,
        normalMap: PixelMap? = null,
        opacityMap: PixelMap? = null
    ) : this(
        albedoMap?.run { Texture2D(this) },
        metallicMap?.run { Texture2D(this) },
        roughnessMap?.run { Texture2D(this) },
        aoMap?.run { Texture2D(this) },
        normalMap?.run { Texture2D(this) },
        opacityMap?.run { Texture2D(this) },
    )


    override fun createVertexAttributes(positionFormat: VectorFormat, vertexColorFormat: ColorFormat?, hasVertexOpacity: Boolean): VertexAttributes {
        val hasAlbedoTexture = albedoSource is TextureColorSource
        val hasMetallicTexture = metallicSource is TextureVector1Source
        val hasRoughnessTexture = roughnessSource is TextureVector1Source
        val hasAoTexture = aoSource is TextureVector1Source
        val hasNormalTexture = normalSource is TextureVector3Source
        val hasOpacityTexture = opacitySource is TextureVector1Source

        val useTexCoord = hasAlbedoTexture || hasMetallicTexture || hasRoughnessTexture || hasAoTexture || hasNormalTexture || hasOpacityTexture
        val textureFormat = if (useTexCoord) VectorFormat.VECTOR_2D else null
        val normalFormat = if (positionFormat == VectorFormat.VECTOR_3D) VectorFormat.VECTOR_3D else null
        val tangentFormat = if (hasNormalTexture) positionFormat else null

        return VertexAttributes(positionFormat, textureFormat, normalFormat, tangentFormat, vertexColorFormat, hasVertexOpacity)
    }
}

