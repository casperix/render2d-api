package ru.casperix.renderer.material

import ru.casperix.renderer.pixel_map.PixelMap


interface Texture

data class Texture2D(
    val map: PixelMap,
    val config: TextureConfig = TextureConfig()
) : Texture

data class Texture2DArray(
    val maps: List<PixelMap>,
    val config: TextureConfig = TextureConfig()
) : Texture

data class TextureCube(
    val minX: PixelMap,
    val maxX: PixelMap,
    val minY: PixelMap,
    val maxY: PixelMap,
    val minZ: PixelMap,
    val maxZ: PixelMap,
    val config: TextureConfig = TextureConfig()
) : Texture


data class TextureConfig(
    val minFilter: TextureFilter = TextureFilter.LINEAR,
    val magFilter: TextureFilter = TextureFilter.LINEAR,
    val uWrap: TextureWrap = TextureWrap.REPEAT,
    val vWrap: TextureWrap = TextureWrap.REPEAT,
    val wWrap: TextureWrap = TextureWrap.REPEAT,
    val useMipMap: Boolean = true,
    val useAnisotropic: Boolean = false,
)

enum class TextureFilter {
    LINEAR,
    NEAREST,
}

enum class TextureWrap {
    CLAMP,
    REPEAT,
}
