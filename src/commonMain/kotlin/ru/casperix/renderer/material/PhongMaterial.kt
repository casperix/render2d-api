package ru.casperix.renderer.material

import ru.casperix.math.color.rgb.RgbColor
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.renderer.vector.vertex.ColorFormat
import ru.casperix.renderer.vector.vertex.VectorFormat
import ru.casperix.renderer.vector.vertex.VertexAttributes

/**
 * Material uses [phong shading](https://en.wikipedia.org/wiki/Phong_shading)
 */
data class PhongMaterial(
    val diffuseSource: ColorSource? = null,
    val specularSource: ColorSource? = null,
    val shinesSource: Vector1Source? = null,
    val ambientSource: ColorSource? = null,
    val normalSource: Vector3Source? = null,
    val opacitySource: Vector1Source? = null,
) : Material {
    constructor(
        diffuseColor: RgbColor? = null,
        specularColor: RgbColor? = null,
        shines: Float? = null,
        ambientColor: RgbColor? = null,
        normal: Vector3f? = null,
        opacity: Float? = null,
    ) : this(
        diffuseColor?.run { ConstantColorSource(toColor3f()) },
        specularColor?.run { ConstantColorSource(toColor3f()) },
        shines?.run { ConstantVector1Source(this) },
        ambientColor?.run { ConstantColorSource(toColor3f()) },
        normal?.run { ConstantVector3Source(this) },
        opacity?.run { ConstantVector1Source(this) },
    )

    constructor(
        diffuseTexture: Texture2D? = null,
        specularTexture: Texture2D? = null,
        ambientTexture: Texture2D? = null,
        normalTexture: Texture2D? = null,
    ) : this(
        diffuseTexture?.run { TextureColorSource(this) },
        specularTexture?.run { TextureColorSource(this) },
        specularTexture?.run { TextureVector1Source(this, 3) },
        ambientTexture?.run { TextureColorSource(this) },
        normalTexture?.run { TextureVector3Source(this) },
        diffuseTexture?.run { TextureVector1Source(this, 3) },
    )

    constructor(
        diffuseMap: PixelMap? = null,
        specularMap: PixelMap? = null,
        ambientMap: PixelMap? = null,
        normalMap: PixelMap? = null,
    ) : this(
        diffuseMap?.run { Texture2D(this) },
        specularMap?.run { Texture2D(this) },
        ambientMap?.run { Texture2D(this) },
        normalMap?.run { Texture2D(this) },
    )

    override fun createVertexAttributes(positionFormat: VectorFormat, vertexColorFormat: ColorFormat?, hasVertexOpacity: Boolean): VertexAttributes {
        val hasDiffuseTexture = diffuseSource is TextureColorSource
        val hasSpecularTexture = specularSource is TextureColorSource
        val hasShinesTexture = shinesSource is TextureVector1Source
        val hasAmbientTexture = ambientSource is TextureColorSource
        val hasNormalTexture = normalSource is TextureVector3Source
        val hasOpacityTexture = opacitySource is TextureVector1Source

        val useTexCoord = hasDiffuseTexture || hasSpecularTexture || hasShinesTexture || hasAmbientTexture || hasNormalTexture || hasOpacityTexture
        val textureFormat = if (useTexCoord) VectorFormat.VECTOR_2D else null
        val normalFormat = if (positionFormat == VectorFormat.VECTOR_3D) VectorFormat.VECTOR_3D else null
        val tangentFormat = if (hasNormalTexture) positionFormat else null

        return VertexAttributes(positionFormat, textureFormat, normalFormat, tangentFormat, vertexColorFormat, hasVertexOpacity)
    }
}

