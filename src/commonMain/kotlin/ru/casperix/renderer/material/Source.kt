package ru.casperix.renderer.material

import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.float32.Vector4f

interface ValueSource


/**
 *  For any values (color, normal, etc.), you can use the source.
 */
sealed interface Vector4Source : ValueSource
data class ConstantVector4Source(val value: Vector4f) : Vector4Source

/**
 * The source is the pixel of the two-dimensional texture, given channelOffset.
 * Let's say we have an "RGB" texture. channelOffset is "2".
 * So the value we are looking for starts with "B". If we need two values, it will be ["B", "A"].
 *
 * If we extract 4 values from a 4-component texture, the channelOffset should be 0.
 * The implication is that we cannot move beyond a single pixel.
 * However, in general, it is not necessary that a texture has at most four channels
 */
data class TextureVector4Source(val value: Texture2D, val channelOffset: Int = 0) : Vector4Source


sealed interface Vector3Source : ValueSource
data class ConstantVector3Source(val value: Vector3f) : Vector3Source
data class TextureVector3Source(val value: Texture2D, val channelOffset: Int = 0) : Vector3Source


sealed interface Vector2Source : ValueSource
data class ConstantVector2Source(val value: Vector2f) : Vector2Source
data class TextureVector2Source(val value: Texture2D, val channelOffset: Int = 0) : Vector2Source


sealed interface Vector1Source : ValueSource
data class ConstantVector1Source(val value: Float) : Vector1Source
data class TextureVector1Source(val value: Texture2D, val channelOffset: Int = 0) : Vector1Source


sealed interface ColorSource : ValueSource
data class ConstantColorSource(val value: RgbColor3f) : ColorSource
data class TextureColorSource(val value: Texture2D, val channelOffset: Int = 0) : ColorSource




