package ru.casperix.renderer.misc

import ru.casperix.math.axis_aligned.float32.Dimension2f
import ru.casperix.math.axis_aligned.float32.Dimension3f
import ru.casperix.math.geometry.Quad
import ru.casperix.math.geometry.Quad2f
import ru.casperix.math.geometry.Triangle
import ru.casperix.math.geometry.Triangle2f
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.quad_matrix.float32.Matrix4f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.vector.vertex.Vertex


fun Float.Companion.fromBytesWithOffset(bytes: UByteArray, offset: Int): Float = fromBytesWithOffset(bytes.asByteArray(), offset)

fun Float.Companion.fromBytesWithOffset(value: ByteArray, offset: Int): Float {
    return Float.fromBits(Int.fromBytesWithOffset(value, offset))
}


fun Int.Companion.fromBytesWithOffset(bytes: UByteArray, offset: Int): Int = fromBytesWithOffset(bytes.asByteArray(), offset)
fun Int.Companion.fromBytesWithOffset(bytes: ByteArray, offset: Int): Int {
    return ((bytes[offset + 3].toInt()) and 0x000000ff) or
            ((bytes[offset + 2].toInt() shl 8) and 0x0000ff00) or
            ((bytes[offset + 1].toInt() shl 16) and 0x00ff0000) or
            ((bytes[offset + 0].toInt() shl 24) and 0xff000000.toInt())
}

fun Matrix4f.Companion.orthographic(dimension: Dimension3f, centered: Boolean, yDown: Boolean): Matrix4f = dimension.run {
    val half = dimension.toVector3f() / 2f
    orthographic(-half.x, half.x, -half.y, half.y, -half.z, half.z)

}

fun Matrix3f.Companion.orthographic(dimension: Dimension2f, centered: Boolean, yDown: Boolean): Matrix3f = dimension.run {
    if (centered) {
        val halfWidth = width / 2
        val halfHeight = height / 2
        if (yDown) {
            orthographic(-halfWidth, halfWidth, halfHeight, -halfHeight)
        } else {
            orthographic(-halfWidth, halfWidth, -halfHeight, halfHeight)
        }
    } else {
        if (yDown) {
            orthographic(0f, width, height, 0f)
        } else {
            orthographic(0f, width, 0f, height)
        }
    }
}



fun List<Triangle2f>.mapTriangles(converter: (Vector2f) -> Vertex): List<Triangle<Vertex>> {
    return map {
        it.convert {
            converter(it)
        }
    }
}

fun List<Quad2f>.mapQuads(converter: (Vector2f) -> Vertex): List<Quad<Vertex>> {
    return map {
        it.convert {
            converter(it)
        }
    }
}