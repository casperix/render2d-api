package ru.casperix.renderer.misc

import ru.casperix.math.vector.float32.Vector2f

data class AlignMode(
    val horizontal: Align,
    val vertical: Align,
) {
    fun getPosition(parentSize: Vector2f, childSize: Vector2f): Vector2f {
        return Vector2f(
            horizontal.getPosition(parentSize.x, childSize.x),
            vertical.getPosition(parentSize.y, childSize.y),
        )
    }

    companion object {
        val LEFT_TOP = AlignMode(Align.MIN, Align.MIN)
        val CENTER_TOP = AlignMode(Align.CENTER, Align.MIN)
        val RIGHT_TOP = AlignMode(Align.MAX, Align.MIN)

        val LEFT_CENTER = AlignMode(Align.MIN, Align.CENTER)
        val CENTER_CENTER = AlignMode(Align.CENTER, Align.CENTER)
        val RIGHT_CENTER = AlignMode(Align.MAX, Align.CENTER)

        val LEFT_BOTTOM = AlignMode(Align.MIN, Align.MAX)
        val CENTER_BOTTOM = AlignMode(Align.CENTER, Align.MAX)
        val RIGHT_BOTTOM = AlignMode(Align.MAX, Align.MAX)
    }
}