package ru.casperix.renderer.misc

enum class Align(val factor: Float) {
    MIN(0.0f),
    CENTER(0.5f),
    MAX(1.0f),
    ;

    fun getPosition(parentSize: Float, childSize: Float): Float {
        if (factor.isNaN()) return 0f
        return (parentSize - childSize) * factor
    }

    override fun toString(): String {
        return when (this) {
            MIN -> "Min"
            CENTER -> "Center"
            MAX -> "Max"
        }
    }
}