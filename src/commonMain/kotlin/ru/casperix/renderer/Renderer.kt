package ru.casperix.renderer

import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.color.rgba.RgbaColor
import ru.casperix.renderer.depth.DepthState
import ru.casperix.renderer.light.Light


interface Renderer {
    var viewPort: Dimension2i
    var scissor: Box2i?
    var lights: List<Light>
    var depthState: DepthState

    fun clearDepth(value: Float = 1f)
    fun clearColor(color: RgbaColor)

    fun flush()
}

