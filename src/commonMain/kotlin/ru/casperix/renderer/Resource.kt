package ru.casperix.renderer

/**
 * For fast compare any unique objects
 */
abstract class Resource {
    //TODO: thread safe
    protected val UID = counter++

    override fun hashCode(): Int {
        return UID
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as Resource

        return UID == other.UID
    }

    companion object {
        private var counter = 0
    }
}