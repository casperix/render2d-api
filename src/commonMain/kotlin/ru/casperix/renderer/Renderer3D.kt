package ru.casperix.renderer

import ru.casperix.math.axis_aligned.float32.Dimension3f
import ru.casperix.math.quad_matrix.float32.Matrix4f
import ru.casperix.renderer.misc.orthographic
import ru.casperix.renderer.vector.VectorGraphic3D
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder3D

interface Renderer3D : Renderer {
    var viewMatrix: Matrix4f
    var projectionMatrix: Matrix4f

    fun draw(graphic: VectorGraphic3D, transform: Matrix4f = Matrix4f.IDENTITY)

    fun draw(transform: Matrix4f = Matrix4f.IDENTITY, graphicBuilder: (VectorGraphicBuilder3D.() -> Unit)) {
        draw(VectorGraphicBuilder3D.build(graphicBuilder), transform)
    }

    fun setOrthographicCamera(dimension: Dimension3f, centered: Boolean = true, yDown: Boolean = true) {
        viewPort = dimension.roundToVector3i().getXY().toDimension2i()
        projectionMatrix = Matrix4f.orthographic(dimension, centered, yDown)
    }
}