package ru.casperix.renderer.vector

import ru.casperix.renderer.material.Material
import ru.casperix.renderer.vector.vertex.VertexData

data class VectorGraphic2D(val shapes: List<VectorShape2D>) {
    constructor(vararg shape: VectorShape2D) : this(shape.toList())
    constructor(material: Material, vertexData: VertexData) : this(VectorShape2D(material, vertexData))

    operator fun plus(other: VectorGraphic2D): VectorGraphic2D {
        return VectorGraphic2D(shapes + other.shapes)
    }

    companion object {
        val EMPTY = VectorGraphic2D(emptyList())
    }
}
