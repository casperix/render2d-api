package ru.casperix.renderer.vector

import ru.casperix.renderer.material.Material
import ru.casperix.renderer.vector.vertex.VertexData

data class VectorGraphic3D(val shapes: List<VectorShape3D>) {
    constructor(vararg shape: VectorShape3D) : this(shape.toList())
    constructor(material: Material, vertexData: VertexData) : this(VectorShape3D(material, vertexData))

    operator fun plus(other: VectorGraphic3D): VectorGraphic3D {
        return VectorGraphic3D(shapes + other.shapes)
    }

    companion object {
        val EMPTY = VectorGraphic3D(emptyList())
    }
}
