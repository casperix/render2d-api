package ru.casperix.renderer.vector

import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.renderer.material.Material
import ru.casperix.renderer.vector.vertex.VectorFormat
import ru.casperix.renderer.vector.vertex.VertexData

data class VectorShape2D(val material: Material, val vertexData: VertexData, val transform: Matrix3f = Matrix3f.IDENTITY) {
    init {
        vertexData.vertices.attributes.apply {
            require(VectorFormat.VECTOR_2D == position) {
                "Vertex position expected ${VectorFormat.VECTOR_2D}, but actual: $position"
            }
        }
    }
}