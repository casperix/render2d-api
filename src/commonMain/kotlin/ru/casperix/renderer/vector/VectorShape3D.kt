package ru.casperix.renderer.vector

import ru.casperix.math.quad_matrix.float32.Matrix4f
import ru.casperix.renderer.material.Material
import ru.casperix.renderer.vector.vertex.VectorFormat
import ru.casperix.renderer.vector.vertex.VertexData

data class VectorShape3D(val material: Material, val vertexData: VertexData, val transform:Matrix4f = Matrix4f.IDENTITY){
    init {
        vertexData.vertices.attributes.apply {
            require(VectorFormat.VECTOR_3D == position) {
                "Vertex position expected ${VectorFormat.VECTOR_3D}, but actual: $position"
            }
        }
    }
}