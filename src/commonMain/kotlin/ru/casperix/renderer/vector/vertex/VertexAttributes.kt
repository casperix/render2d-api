package ru.casperix.renderer.vector.vertex

data class VertexAttributes(
    val position: VectorFormat? = null,
    val textureCoord: VectorFormat? = null,
    val normal: VectorFormat? = null,
    val tangent: VectorFormat? = null,
    val color: ColorFormat? = null,
    val hasOpacity: Boolean = false,
) {
    fun calculateVertexSize(): Int {
        var offset = 0
        offset += position?.components ?: 0
        offset += textureCoord?.components ?: 0
        offset += normal?.components ?: 0
        offset += tangent?.components ?: 0
        offset += color?.components ?: 0
        if (hasOpacity) offset += 1
        return offset
    }
}