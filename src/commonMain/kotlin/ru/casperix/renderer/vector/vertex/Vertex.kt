package ru.casperix.renderer.vector.vertex

import kotlinx.serialization.Serializable
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.renderer.vector.vertex.FloatEncoder.encode
import ru.casperix.renderer.vector.vertex.FloatEncoder.encodeTo


/**
 * You can use Vertex...
 * But if you need good performance, then use [VectorGraphic.build]
 */
@Serializable
data class Vertex(val attributes: List<VertexAttribute>) {
    constructor(vararg attributes: VertexAttribute) : this(attributes.toList())
}

interface VertexAttribute {
    fun encodeTo(target: FloatArray, offset: Int): Int
    fun encode(): FloatArray
}

@Serializable
data class VertexPosition2(val value: Vector2f) : VertexAttribute {
    override fun encodeTo(target: FloatArray, offset: Int): Int = value.encodeTo(target, offset)
    override fun encode(): FloatArray = value.encode()
}

@Serializable
data class VertexPosition3(val value: Vector3f) : VertexAttribute {
    override fun encodeTo(target: FloatArray, offset: Int): Int = value.encodeTo(target, offset)
    override fun encode(): FloatArray = value.encode()
}


@Serializable
data class VertexTextureCoord2(val value: Vector2f) : VertexAttribute {
    override fun encodeTo(target: FloatArray, offset: Int): Int = value.encodeTo(target, offset)
    override fun encode(): FloatArray = value.encode()
}

@Serializable
data class VertexTextureCoord3(val value: Vector3f) : VertexAttribute {
    override fun encodeTo(target: FloatArray, offset: Int): Int = value.encodeTo(target, offset)
    override fun encode(): FloatArray = value.encode()
}


@Serializable
data class VertexNormal2(val value: Vector2f) : VertexAttribute {
    override fun encodeTo(target: FloatArray, offset: Int): Int = value.encodeTo(target, offset)
    override fun encode(): FloatArray = value.encode()
}

@Serializable
data class VertexNormal3(val value: Vector3f) : VertexAttribute {
    override fun encodeTo(target: FloatArray, offset: Int): Int = value.encodeTo(target, offset)
    override fun encode(): FloatArray = value.encode()
}

@Serializable
data class VertexTangent2(val value: Vector2f) : VertexAttribute {
    override fun encodeTo(target: FloatArray, offset: Int): Int = value.encodeTo(target, offset)
    override fun encode(): FloatArray = value.encode()
}

@Serializable
data class VertexTangent3(val value: Vector3f) : VertexAttribute {
    override fun encodeTo(target: FloatArray, offset: Int): Int = value.encodeTo(target, offset)
    override fun encode(): FloatArray = value.encode()
}


@Serializable
data class VertexColor(val color: RgbColor3f) : VertexAttribute {
    override fun encodeTo(target: FloatArray, offset: Int): Int = color.encodeTo(target, offset)
    override fun encode(): FloatArray = color.encode()
}

@Serializable
data class VertexOpacity(val opacity: Float) : VertexAttribute {
    override fun encodeTo(target: FloatArray, offset: Int): Int = opacity.encodeTo(target, offset)
    override fun encode(): FloatArray = opacity.encode()
}


