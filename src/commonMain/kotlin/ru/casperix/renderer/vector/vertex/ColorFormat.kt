package ru.casperix.renderer.vector.vertex

enum class ColorFormat(val components: Int, val directOrder: Boolean) {
    RGB(3, true),
    BGR(3, false),
}