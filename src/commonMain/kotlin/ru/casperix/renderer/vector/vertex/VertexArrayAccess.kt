package ru.casperix.renderer.vector.vertex

import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f

/**
 *  Create new buffer: [VertexArray]
 */
interface VertexArrayAccess {
    val size: Int

    val attributes: VertexAttributes

    fun getPosition2(vertexId: Int): Vector2f
    fun getPosition3(vertexId: Int): Vector3f
    fun getNormal2(vertexId: Int): Vector2f
    fun getNormal3(vertexId: Int): Vector3f
    fun getTextureCoord2(vertexId: Int): Vector2f
    fun getTextureCoord3(vertexId: Int): Vector3f
    fun getTangent2(vertexId: Int): Vector2f
    fun getTangent3(vertexId: Int): Vector3f
    fun getColor(vertexId: Int): RgbColor3f
    fun getOpacity(vertexId: Int): Float


    fun setPosition2(vertexId: Int, x: Float, y: Float): Boolean
    fun setPosition3(vertexId: Int, x: Float, y: Float, z: Float): Boolean
    fun setNormal2(vertexId: Int, x: Float, y: Float): Boolean
    fun setNormal3(vertexId: Int, x: Float, y: Float, z: Float): Boolean
    fun setTextureCoord2(vertexId: Int, u: Float, v: Float): Boolean
    fun setTextureCoord3(vertexId: Int, u: Float, v: Float, w:Float): Boolean
    fun setTangent2(vertexId: Int, x: Float, y: Float): Boolean
    fun setTangent3(vertexId: Int, x: Float, y: Float, z:Float): Boolean
    fun setColor(vertexId: Int, red: Float, green: Float, blue: Float): Boolean
    fun setOpacity(vertexId: Int, opacity: Float): Boolean

    fun setVertex(vertexId: Int, vertex: Vertex) {
        vertex.attributes.forEach {
            when (it) {
                is VertexPosition2 -> setPosition2(vertexId, it.value.x, it.value.y)
                is VertexPosition3 -> setPosition3(vertexId, it.value.x, it.value.y, it.value.z)
                is VertexTextureCoord2 -> setTextureCoord2(vertexId, it.value.x, it.value.y)
                is VertexTangent2 -> setTangent2(vertexId, it.value.x, it.value.y)
                is VertexColor -> it.color.apply { setColor(vertexId, red, green, blue) }
                is VertexOpacity -> it.opacity.apply { setOpacity(vertexId, this) }
            }
        }
    }

    fun setPosition2(vertexId: Int, value: Vector2f) = value.apply {
        setPosition2(vertexId, x, y)
    }

    fun setPosition3(vertexId: Int, value: Vector3f) = value.apply {
        setPosition3(vertexId, x, y, z)
    }

    fun setTextureCoord2(vertexId: Int, value: Vector2f) = value.apply {
        setTextureCoord2(vertexId, x, y)
    }

    fun setTangent2(vertexId: Int, value: Vector2f) = value.apply {
        setTangent2(vertexId, x, y)
    }

    fun setColor(vertexId: Int, color: RgbColor3f) = color.apply {
        setColor(vertexId, red, green, blue)
    }

}

