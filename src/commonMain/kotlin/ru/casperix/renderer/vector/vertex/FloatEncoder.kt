package ru.casperix.renderer.vector.vertex

import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f

internal object FloatEncoder {
    fun Vector2f.encodeTo(target: FloatArray, offset: Int) = run {
        target[offset] = x
        target[offset + 1] = y
        offset + 2
    }

    fun Vector2f.encode(): FloatArray = run {
        return floatArrayOf(x, y)
    }


    fun Vector3f.encodeTo(target: FloatArray, offset: Int) = run {
        target[offset] = x
        target[offset + 1] = y
        target[offset + 2] = z
        offset + 3
    }

    fun Vector3f.encode(): FloatArray = floatArrayOf(x, y, z)


    fun RgbColor3f.encodeTo(target: FloatArray, offset: Int) = run {
        target[offset] = red
        target[offset + 1] = green
        target[offset + 2] = blue
        offset + 3
    }

    fun RgbColor3f.encode(): FloatArray = floatArrayOf(red, green, blue)

    fun Float.encodeTo(target: FloatArray, offset: Int) = run {
        target[offset] = this
        offset + 1
    }

    fun Float.encode(): FloatArray = floatArrayOf(this)

}