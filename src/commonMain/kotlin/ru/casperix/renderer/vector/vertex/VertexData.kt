package ru.casperix.renderer.vector.vertex

import ru.casperix.renderer.Resource
import ru.casperix.renderer.vector.vertex.VertexArray


class VertexData(val indices: UIntArray, val vertices: VertexArray) : Resource() {
    fun isEmpty(): Boolean {
        return indices.isEmpty()
    }
}

