package ru.casperix.renderer.vector.vertex

enum class VectorFormat(val components:Int) {
    VECTOR_2D(2),
    VECTOR_3D(3),
}