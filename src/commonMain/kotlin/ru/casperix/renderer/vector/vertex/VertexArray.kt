package ru.casperix.renderer.vector.vertex

import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f

class VertexArray(
    override val size: Int,
    override val attributes: VertexAttributes,
    val data: FloatArray = FloatArray(size * attributes.calculateVertexSize()),
) : VertexArrayAccess {

    constructor(
        size: Int, position: VectorFormat,
        textureCoord: VectorFormat? = null,
        normal: VectorFormat? = null,
        tangent: VectorFormat? = null,
        color: ColorFormat? = null
    ) : this(size, VertexAttributes(position, textureCoord, normal, tangent, color))

    val positionOffset: Int
    val textureCoordOffset: Int
    val normalOffset: Int
    val tangentOffset: Int
    val colorOffset: Int
    val opacityOffset: Int

    val vertexSize = attributes.calculateVertexSize()

    init {
        if (data.size != size * vertexSize) {
            throw Exception("Invalid array size. Expected: ${size * vertexSize}, but actual is: ${data.size}")
        }

        var offset = 0

        fun addVectorOffset(format: VectorFormat?) = when (format) {
            null -> Int.MAX_VALUE
            else -> offset.apply { offset += format.components }
        }

        fun addColorOffset(format: ColorFormat?) = when (format) {
            null -> Int.MAX_VALUE
            else -> offset.apply { offset += format.components }
        }

        fun addBooleanOffset(value: Boolean) = when (value) {
            false -> Int.MAX_VALUE
            true -> offset.apply { offset += 1 }
        }

        positionOffset = addVectorOffset(attributes.position)
        textureCoordOffset = addVectorOffset(attributes.textureCoord)
        normalOffset = addVectorOffset(attributes.normal)
        tangentOffset = addVectorOffset(attributes.tangent)
        colorOffset = addColorOffset(attributes.color)
        opacityOffset = addBooleanOffset(attributes.hasOpacity)
    }

    private fun setVector2(vertexId: Int, localOffset: Int, x: Float, y: Float) {
        val offset = vertexId * vertexSize + localOffset
        data[offset] = x
        data[offset + 1] = y
    }

    private fun getVector2(vertexId: Int, localOffset: Int): Vector2f {
        val offset = vertexId * vertexSize + localOffset
        val x = data[offset + 0]
        val y = data[offset + 1]
        return Vector2f(x, y)
    }

    private fun getVector3(vertexId: Int, localOffset: Int): Vector3f {
        val offset = vertexId * vertexSize + localOffset
        val x = data[offset + 0]
        val y = data[offset + 1]
        val z = data[offset + 2]
        return Vector3f(x, y, z)
    }

    private fun getSingle(vertexId: Int, localOffset: Int): Float {
        val offset = vertexId * vertexSize + localOffset
        return data[offset]
    }

    private fun setVector3(vertexId: Int, localOffset: Int, x: Float, y: Float, z: Float) {
        val offset = vertexId * vertexSize + localOffset
        data[offset] = x
        data[offset + 1] = y
        data[offset + 2] = z
    }

    override fun setPosition2(vertexId: Int, x: Float, y: Float) = if (attributes.position == VectorFormat.VECTOR_2D) {
        setVector2(vertexId, positionOffset, x, y)
        true
    } else {
        false
    }

    override fun getPosition2(vertexId: Int): Vector2f {
        if (attributes.position != VectorFormat.VECTOR_2D) {
            throw Exception("This buffer not support attribute 'Position2'")
        }
        return getVector2(vertexId, positionOffset)
    }

    override fun setPosition3(vertexId: Int, x: Float, y: Float, z: Float) = if (attributes.position == VectorFormat.VECTOR_3D) {
        setVector3(vertexId, positionOffset, x, y, z)
        true
    } else {
        false
    }

    override fun getPosition3(vertexId: Int): Vector3f {
        if (attributes.position != VectorFormat.VECTOR_3D) {
            throw Exception("This buffer not support attribute 'Position3'")
        }
        return getVector3(vertexId, positionOffset)
    }

    override fun setNormal2(vertexId: Int, x: Float, y: Float) = if (attributes.normal == VectorFormat.VECTOR_2D) {
        setVector2(vertexId, normalOffset, x, y)
        true
    } else {
        false
    }

    override fun getNormal2(vertexId: Int): Vector2f {
        if (attributes.normal != VectorFormat.VECTOR_2D) {
            throw Exception("This buffer not support attribute 'Position2'")
        }
        return getVector2(vertexId, normalOffset)
    }

    override fun setNormal3(vertexId: Int, x: Float, y: Float, z: Float) = if (attributes.normal == VectorFormat.VECTOR_3D) {
        setVector3(vertexId, normalOffset, x, y, z)
        true
    } else {
        false
    }


    override fun getNormal3(vertexId: Int): Vector3f {
        if (attributes.normal != VectorFormat.VECTOR_3D) {
            throw Exception("This buffer not support attribute 'Position2'")
        }
        return getVector3(vertexId, normalOffset)
    }

    override fun setTextureCoord2(vertexId: Int, u: Float, v: Float) = if (attributes.textureCoord == VectorFormat.VECTOR_2D) {
        setVector2(vertexId, textureCoordOffset, u, v)
        true
    } else {
        false
    }

    override fun getTextureCoord2(vertexId: Int): Vector2f {
        if (attributes.textureCoord != VectorFormat.VECTOR_2D) {
            throw Exception("This buffer not support attribute 'TextureCoord'")
        }
        return getVector2(vertexId, textureCoordOffset)
    }

    override fun setTextureCoord3(vertexId: Int, x: Float, y: Float, z: Float) = if (attributes.textureCoord == VectorFormat.VECTOR_3D) {
        setVector3(vertexId, textureCoordOffset, x, y, z)
        true
    } else {
        false
    }

    override fun getTextureCoord3(vertexId: Int): Vector3f {
        if (attributes.textureCoord != VectorFormat.VECTOR_3D) {
            throw Exception("This buffer not support attribute 'Position3'")
        }
        return getVector3(vertexId, textureCoordOffset)
    }


    override fun setTangent2(vertexId: Int, x: Float, y: Float) = if (attributes.tangent == VectorFormat.VECTOR_2D) {
        setVector2(vertexId, tangentOffset, x, y)
        true
    } else {
        false
    }

    override fun getTangent2(vertexId: Int): Vector2f {
        if (attributes.tangent != VectorFormat.VECTOR_2D) {
            throw Exception("This buffer not support attribute 'Tangent'")
        }
        return getVector2(vertexId, tangentOffset)
    }


    override fun setTangent3(vertexId: Int, x: Float, y: Float, z: Float) = if (attributes.tangent == VectorFormat.VECTOR_3D) {
        setVector3(vertexId, tangentOffset, x, y, z)
        true
    } else {
        false
    }

    override fun getTangent3(vertexId: Int): Vector3f {
        if (attributes.tangent != VectorFormat.VECTOR_3D) {
            throw Exception("This buffer not support attribute 'Tangent'")
        }
        return getVector3(vertexId, tangentOffset)
    }


    override fun setOpacity(vertexId: Int, opacity: Float) = if (attributes.hasOpacity) {
        getSingle(vertexId, opacityOffset)
        true
    } else {
        false
    }

    override fun getOpacity(vertexId: Int): Float {
        if (!attributes.hasOpacity) {
            throw Exception("This buffer not support attribute 'Position3'")
        }
        val offset = vertexId * vertexSize + opacityOffset
        return data[offset + 0]
    }

    override fun setColor(vertexId: Int, red: Float, green: Float, blue: Float): Boolean {
        when (attributes.color) {
            ColorFormat.RGB -> setVector3(vertexId, colorOffset, red, green, blue)
            ColorFormat.BGR -> setVector3(vertexId, colorOffset, blue, green, red)
            null -> return false
        }

        return true
    }

    override fun getColor(vertexId: Int): RgbColor3f {
        val offset = vertexId * vertexSize + colorOffset
        return when (attributes.color) {

            ColorFormat.RGB -> {
                RgbColor3f(
                    data[offset],
                    data[offset + 1],
                    data[offset + 2],
                )
            }

            ColorFormat.BGR -> {
                RgbColor3f(
                    data[offset + 2],
                    data[offset + 1],
                    data[offset],
                )
            }

            null -> {
                throw Exception("This buffer not support attribute 'Color'")
            }
        }
    }

    companion object {
        val EMPTY = VertexArray(0, VertexAttributes())

        fun from(vertexList: List<Vertex>, defaultColorFormat: ColorFormat = ColorFormat.RGB): VertexArray {
            var attributes = VertexAttributes()

            vertexList.forEach {
                it.attributes.forEach {
                    when (it) {
                        is VertexPosition2 -> attributes = attributes.copy(position = VectorFormat.VECTOR_2D)
                        is VertexPosition3 -> attributes = attributes.copy(position = VectorFormat.VECTOR_3D)
                        is VertexTextureCoord2 -> attributes = attributes.copy(textureCoord = VectorFormat.VECTOR_2D)
                        is VertexTangent2 -> attributes = attributes.copy(tangent = VectorFormat.VECTOR_2D)
                        is VertexColor -> attributes = attributes.copy(color = defaultColorFormat)
                    }
                }
            }

            return VertexArray(vertexList.size, attributes).apply {
                vertexList.forEachIndexed { vertexId, vertex ->
                    setVertex(vertexId, vertex)
                }
            }
        }
    }

}