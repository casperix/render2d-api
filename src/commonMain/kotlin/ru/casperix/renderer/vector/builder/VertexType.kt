package ru.casperix.renderer.vector.builder

import ru.casperix.renderer.material.Material
import ru.casperix.renderer.vector.vertex.ColorFormat
import ru.casperix.renderer.vector.vertex.VectorFormat
import ru.casperix.renderer.vector.vertex.VertexAttributes

data class VertexType(val material: Material, val colorFormat: ColorFormat?, val hasOpacity:Boolean, val position:VectorFormat) {
    fun createVertexAttributes(): VertexAttributes = material.createVertexAttributes(position, colorFormat, hasOpacity)
}