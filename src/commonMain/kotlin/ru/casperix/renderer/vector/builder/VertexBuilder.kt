package ru.casperix.renderer.vector.builder

import ru.casperix.math.color.Color
import ru.casperix.math.color.rgb.RgbColor
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f

interface VertexBuilder {
    fun setPosition2(x: Float, y: Float)
    fun setPosition3(x: Float, y: Float, z: Float)
    fun setNormal2(x: Float, y: Float)
    fun setNormal3(x: Float, y: Float, z: Float)

    fun setTextureCoord2(u: Float, v: Float)
    fun setTextureCoord3(u: Float, v: Float, w: Float)
    fun setTangent2(x: Float, y: Float)
    fun setTangent3(x: Float, y: Float, z: Float)
    fun setRGB(red: Float, green: Float, blue: Float)
    fun setOpacity(opacity: Float)

    fun setPosition2(value: Vector2f) = value.apply {
        setPosition2(x, y)
    }

    fun setPosition3(value: Vector3f) = value.apply {
        setPosition3(x, y, z)
    }

    fun setNormal2(value: Vector2f) = value.apply {
        setNormal2(x, y)
    }

    fun setNormal3(value: Vector3f) = value.apply {
        setNormal3(x, y, z)
    }

    fun setTextureCoord2(value: Vector2f) = value.apply {
        setTextureCoord2(x, y)
    }

    fun setTextureCoord3(value: Vector3f) = value.apply {
        setTextureCoord3(x, y, z)
    }

    fun setTangent2(value: Vector2f) = value.apply {
        setTangent2(x, y)
    }

    fun setTangent3(value: Vector3f) = value.apply {
        setTangent3(x, y, z)
    }

    fun setRGB(color: RgbColor) = color.toColor3f().apply {
        setRGB(red, green, blue)
    }

    fun setColor(color:Color) = color.toRGBA().toColor4f().apply {
        setRGB(red, green, blue)
        setOpacity(alpha)
    }

}