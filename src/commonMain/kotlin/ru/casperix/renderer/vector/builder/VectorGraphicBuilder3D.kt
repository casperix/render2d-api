package ru.casperix.renderer.vector.builder

import ru.casperix.math.quad_matrix.float32.Matrix4f
import ru.casperix.renderer.material.Material
import ru.casperix.renderer.vector.VectorGraphic3D
import ru.casperix.renderer.vector.VectorShape2D
import ru.casperix.renderer.vector.VectorShape3D
import ru.casperix.renderer.vector.vertex.ColorFormat
import ru.casperix.renderer.vector.vertex.VectorFormat

class VectorGraphicBuilder3D : VectorGraphicBuilderExtension3D {
    private val vertexBuilderMap = mutableMapOf<VertexType, VertexDataBuilder>()
    private val additionalShapes = mutableListOf<VectorShape3D>()

    private fun getVertexBuilder(material: Material, vertexColor: ColorFormat? = null, hasOpacity: Boolean = false): VertexDataBuilder {
        val type = VertexType(material, vertexColor, hasOpacity, VectorFormat.VECTOR_3D)
        return vertexBuilderMap.getOrPut(type) {
            val attributes = type.createVertexAttributes()
            VertexDataBuilder(attributes)
        }
    }

    fun add(graphic: VectorGraphic3D, transform: Matrix4f = Matrix4f.IDENTITY) {
        add(graphic.shapes, transform)
    }

    fun add(shapes: List<VectorShape3D>, transform: Matrix4f = Matrix4f.IDENTITY) {
        additionalShapes += if (transform == Matrix4f.IDENTITY) {
            shapes
        } else {
            shapes.map { VectorShape3D(it.material, it.vertexData, it.transform * transform) }
        }
    }

    override fun add(material: Material, vertexColor: ColorFormat?, hasOpacity: Boolean, geometryBuilder: VertexDataBuilder.() -> Unit) {
        getVertexBuilder(material, vertexColor, hasOpacity).build(geometryBuilder)
    }

    fun build(): VectorGraphic3D {
        return VectorGraphic3D(additionalShapes + vertexBuilderMap.map { (type, builder) ->
            val data = builder.build()
            VectorShape3D(type.material, data)
        })
    }

    companion object {
        fun build(builder: VectorGraphicBuilder3D.() -> Unit) =
            VectorGraphicBuilder3D().apply(builder).build()
    }
}