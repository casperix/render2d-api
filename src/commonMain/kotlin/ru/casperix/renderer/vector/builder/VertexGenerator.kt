package ru.casperix.renderer.vector.builder

import ru.casperix.math.vector.float32.Vector2f

interface VertexGenerator {
    fun createTextureCoord():Vector2f?
    fun createTangent():Vector2f?
    fun createColor():Vector2f?
}