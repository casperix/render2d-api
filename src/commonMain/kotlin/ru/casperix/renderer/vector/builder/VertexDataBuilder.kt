package ru.casperix.renderer.vector.builder

import ru.casperix.math.color.Color
import ru.casperix.math.geometry.Quad
import ru.casperix.math.geometry.Triangle
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.material.Material
import ru.casperix.renderer.vector.vertex.VertexData
import ru.casperix.renderer.vector.vertex.*

class VertexDataBuilder(
    vertexAttributes: VertexAttributes,
    initialIndexAmount: Int = 16,
    initialVertexAmount: Int = 16,
) : VertexDataBuilderExtension {

    companion object {
        fun build(material: Material, positionFormat: VectorFormat, colorFormat: ColorFormat? = null, hasVertexOpacity:Boolean = false, builder: (VertexDataBuilder.() -> Unit)) =
            build(VertexType(material, colorFormat, hasVertexOpacity, positionFormat), builder)

        fun build(type: VertexType, builder: (VertexDataBuilder.() -> Unit)): VertexData =
            build(type.createVertexAttributes(), builder)

        fun build(attributes: VertexAttributes, builder: (VertexDataBuilder.() -> Unit)): VertexData =
            VertexDataBuilder(attributes).build(builder)

        fun generateQuadIndices(quadAmount: Int): UIntArray {
            val array = UIntArray(quadAmount * 6)

            (0 until quadAmount).forEach { quadIndex ->
                val indexOffset = quadIndex * 6
                val vertexOffset = quadIndex.toUInt() * 4u

                array[indexOffset + 0] = vertexOffset + 0u
                array[indexOffset + 1] = vertexOffset + 1u
                array[indexOffset + 2] = vertexOffset + 2u
                array[indexOffset + 3] = vertexOffset + 0u
                array[indexOffset + 4] = vertexOffset + 2u
                array[indexOffset + 5] = vertexOffset + 3u
            }

            return array
        }

        fun generateTriangleIndices(triangleAmount: Int): UIntArray {
            return UIntArray(triangleAmount * 3) { it.toUInt() }
        }
    }

    constructor(
        position: VectorFormat? = null,
        textureCoord: VectorFormat? = null,
        normal:VectorFormat? = null,
        tangent:VectorFormat? = null,
        color: ColorFormat? = null,
        hasOpacity:Boolean = false,
    ) : this(VertexAttributes(position, textureCoord, normal, tangent, color, hasOpacity))

    private val indexBuilder = IndexArrayBuilder(initialIndexAmount)
    private val vertexBuilder = VertexArrayBuilder(vertexAttributes, initialVertexAmount)

    /**
     * Clear accumulated geometry
     */
    fun clear() {
        indexBuilder.clear()
        vertexBuilder.clear()
    }

    fun build(builder: (VertexDataBuilder.() -> Unit)? = null): VertexData {
        builder?.invoke(this)
        return VertexData(indexBuilder.build(), vertexBuilder.build())
    }

    override fun addTriangleVertex(triangle: Triangle<Vertex>) {
        val v0 = addVertex(triangle.v0)
        val v1 = addVertex(triangle.v1)
        val v2 = addVertex(triangle.v2)
        addTriangleIndices(v0, v1, v2)
    }

    override fun addQuadVertex(quad: Quad<Vertex>) {
        val v0 = addVertex(quad.v0)
        val v1 = addVertex(quad.v1)
        val v2 = addVertex(quad.v2)
        val v3 = addVertex(quad.v3)
        addQuadIndices(v0, v1, v2, v3)
    }

    override fun addTriangle(v0: VertexBuilder.() -> Unit, v1: VertexBuilder.() -> Unit, v2: VertexBuilder.() -> Unit) {
        val i0 = vertexBuilder.next()
        v0.invoke(vertexBuilder)

        val i1 = vertexBuilder.next()
        v1.invoke(vertexBuilder)

        val i2 = vertexBuilder.next()
        v2.invoke(vertexBuilder)

        addTriangleIndices(i0, i1, i2)
    }

    override fun addQuad(v0: VertexBuilder.() -> Unit, v1: VertexBuilder.() -> Unit, v2: VertexBuilder.() -> Unit, v3: VertexBuilder.() -> Unit) {
        val i0 = vertexBuilder.next()
        v0.invoke(vertexBuilder)

        val i1 = vertexBuilder.next()
        v1.invoke(vertexBuilder)

        val i2 = vertexBuilder.next()
        v2.invoke(vertexBuilder)

        val i3 = vertexBuilder.next()
        v3.invoke(vertexBuilder)

        addQuadIndices(i0, i1, i2, i3)
    }

    override fun addTriangle(builder: VertexBuilder.(Int) -> Unit) {
        val i0 = vertexBuilder.next()
        builder.invoke(vertexBuilder, 0)

        val i1 = vertexBuilder.next()
        builder.invoke(vertexBuilder, 1)

        val i2 = vertexBuilder.next()
        builder.invoke(vertexBuilder, 2)

        addTriangleIndices(i0, i1, i2)
    }

    override fun addQuad(builder: VertexBuilder.(Int) -> Unit) {
        val i0 = vertexBuilder.next()
        builder.invoke(vertexBuilder, 0)

        val i1 = vertexBuilder.next()
        builder.invoke(vertexBuilder, 1)

        val i2 = vertexBuilder.next()
        builder.invoke(vertexBuilder, 2)

        val i3 = vertexBuilder.next()
        builder.invoke(vertexBuilder, 3)

        addQuadIndices(i0, i1, i2, i3)
    }

//    override fun addQuad2f(p00: Vector2f, p10: Vector2f, p11: Vector2f, p01: Vector2f) {
//        val v0 = vertexBuilder.next()
//        vertexBuilder.setPosition2(p00)
//
//        val v1 = vertexBuilder.next()
//        vertexBuilder.setPosition2(p10)
//
//        val v2 = vertexBuilder.next()
//        vertexBuilder.setPosition2(p11)
//
//        val v3 = vertexBuilder.next()
//        vertexBuilder.setPosition2(p01)
//
//        addQuadIndices(v0, v1, v2, v3)
//    }

    override fun addQuad2f(p00: Vector2f, p10: Vector2f, p11: Vector2f, p01: Vector2f, c00: Color, c10: Color, c11: Color, c01: Color) {
        val v0 = vertexBuilder.next()
        vertexBuilder.setPosition2(p00)
        vertexBuilder.setColor(c00)

        val v1 = vertexBuilder.next()
        vertexBuilder.setPosition2(p10)
        vertexBuilder.setColor(c10)

        val v2 = vertexBuilder.next()
        vertexBuilder.setPosition2(p11)
        vertexBuilder.setColor(c11)

        val v3 = vertexBuilder.next()
        vertexBuilder.setPosition2(p01)
        vertexBuilder.setColor(c01)

        addQuadIndices(v0, v1, v2, v3)
    }

    override fun addQuad2f(
        p00: Vector2f,
        p10: Vector2f,
        p11: Vector2f,
        p01: Vector2f,
        t00: Vector2f,
        t10: Vector2f,
        t11: Vector2f,
        t01: Vector2f,
        generateTangent: Boolean
    ) {
        val tangent = if (generateTangent) {
            (p10 - p00).normalize()
        } else null

        val v0 = vertexBuilder.next()
        vertexBuilder.setPosition2(p00)
        vertexBuilder.setTextureCoord2(t00)
        if (tangent != null) vertexBuilder.setTangent2(tangent)

        val v1 = vertexBuilder.next()
        vertexBuilder.setPosition2(p10)
        vertexBuilder.setTextureCoord2(t10)
        if (tangent != null) vertexBuilder.setTangent2(tangent)

        val v2 = vertexBuilder.next()
        vertexBuilder.setPosition2(p11)
        vertexBuilder.setTextureCoord2(t11)
        if (tangent != null) vertexBuilder.setTangent2(tangent)

        val v3 = vertexBuilder.next()
        vertexBuilder.setPosition2(p01)
        vertexBuilder.setTextureCoord2(t01)
        if (tangent != null) vertexBuilder.setTangent2(tangent)

        addQuadIndices(v0, v1, v2, v3)
    }


    override fun addTriangle2f(pos0: Vector2f, pos1: Vector2f, pos2: Vector2f) {
        val v0 = vertexBuilder.next()
        vertexBuilder.setPosition2(pos0)

        val v1 = vertexBuilder.next()
        vertexBuilder.setPosition2(pos1)

        val v2 = vertexBuilder.next()
        vertexBuilder.setPosition2(pos2)

        addTriangleIndices(v0, v1, v2)
    }

    override fun addTriangle2f(pos0: Vector2f, pos1: Vector2f, pos2: Vector2f, color: Color) {
        val c = color.toRGB()

        val v0 = vertexBuilder.next()
        vertexBuilder.setPosition2(pos0)
        vertexBuilder.setColor(c)

        val v1 = vertexBuilder.next()
        vertexBuilder.setPosition2(pos1)
        vertexBuilder.setColor(c)

        val v2 = vertexBuilder.next()
        vertexBuilder.setPosition2(pos2)
        vertexBuilder.setColor(c)

        addTriangleIndices(v0, v1, v2)
    }

    override fun addTriangle2f(p0: Vector2f, p1: Vector2f, p2: Vector2f, t0: Vector2f, t1: Vector2f, t2: Vector2f, generateTangent: Boolean) {
        val tangent = if (generateTangent) {
            (p1 - p0).normalize()
        } else null

        val v0 = vertexBuilder.next()
        vertexBuilder.setPosition2(p0)
        vertexBuilder.setTextureCoord2(t0)
        if (tangent != null) vertexBuilder.setTangent2(tangent)

        val v1 = vertexBuilder.next()
        vertexBuilder.setPosition2(p1)
        vertexBuilder.setTextureCoord2(t1)
        if (tangent != null) vertexBuilder.setTangent2(tangent)

        val v2 = vertexBuilder.next()
        vertexBuilder.setPosition2(p2)
        vertexBuilder.setTextureCoord2(t2)
        if (tangent != null) vertexBuilder.setTangent2(tangent)

        addTriangleIndices(v0, v1, v2)
    }


    private fun addQuadIndices(v0: UInt, v1: UInt, v2: UInt, v3: UInt) {
        indexBuilder.addQuadIndex(v0, v1, v2, v3)
    }

    private fun addTriangleIndices(v0: UInt, v1: UInt, v2: UInt) {
        indexBuilder.addTriangleIndices(v0, v1, v2)
    }

    private fun addVertex(vertex: Vertex): UInt {
        val index = vertexBuilder.next()
        vertex.attributes.forEach { vertexAttribute ->
            when (vertexAttribute) {
                is VertexPosition2 -> vertexBuilder.setPosition2(vertexAttribute.value)
                is VertexPosition3 -> vertexBuilder.setPosition3(vertexAttribute.value)
                is VertexNormal2 -> vertexBuilder.setNormal2(vertexAttribute.value)
                is VertexNormal3 -> vertexBuilder.setNormal3(vertexAttribute.value)
                is VertexTextureCoord2 -> vertexBuilder.setTextureCoord2(vertexAttribute.value)
                is VertexTextureCoord3 -> vertexBuilder.setTextureCoord3(vertexAttribute.value)
                is VertexTangent2 -> vertexBuilder.setTangent2(vertexAttribute.value)
                is VertexTangent3 -> vertexBuilder.setTangent3(vertexAttribute.value)
                is VertexColor -> vertexBuilder.setRGB(vertexAttribute.color)
                is VertexOpacity -> vertexBuilder.setOpacity(vertexAttribute.opacity)
            }
        }
        return index
    }


}

