package ru.casperix.renderer.vector.builder

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.axis_aligned.float32.Box3f
import ru.casperix.math.color.Color
import ru.casperix.math.geometry.Triangle3f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.toQuad
import ru.casperix.renderer.material.Material
import ru.casperix.renderer.material.SimpleMaterial
import ru.casperix.renderer.vector.vertex.ColorFormat

interface VectorGraphicBuilderExtension3D {
    companion object {
        val IDENTITY_TRIANGLE = Triangle3f(Vector3f.ZERO, Vector3f.X, Vector3f.Y)
        val IDENTITY_BOX = Box3f(Vector3f.ZERO, Vector3f.ONE)
        val IDENTITY_FLAT_RECT = Box2f.ONE
        val IDENTITY_FLAT_QUAD = IDENTITY_FLAT_RECT.toQuad()
    }

    fun add(material: Material, vertexColor: ColorFormat? = null, hasOpacity: Boolean = false, geometryBuilder: VertexDataBuilder.() -> Unit)

    fun add(color: Color, vertexColor: ColorFormat? = null, hasOpacity: Boolean = false, geometryBuilder: VertexDataBuilder.() -> Unit) =
        add(SimpleMaterial(color), vertexColor, hasOpacity, geometryBuilder)


}