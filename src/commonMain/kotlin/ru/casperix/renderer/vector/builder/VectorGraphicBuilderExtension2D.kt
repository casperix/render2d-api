package ru.casperix.renderer.vector.builder

import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Color
import ru.casperix.math.curve.float32.Curve2f
import ru.casperix.math.geometry.Line2f
import ru.casperix.math.geometry.Polygon2f
import ru.casperix.math.geometry.Quad2f
import ru.casperix.math.geometry.Triangle2f
import ru.casperix.math.geometry.builder.ArrowMode
import ru.casperix.math.geometry.builder.BorderMode
import ru.casperix.math.geometry.builder.Triangulator
import ru.casperix.math.geometry.builder.UniformArrowMode
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.toQuad
import ru.casperix.renderer.material.Material
import ru.casperix.renderer.material.SimpleMaterial
import ru.casperix.renderer.vector.vertex.ColorFormat

interface VectorGraphicBuilderExtension2D {
    companion object {
        val IDENTITY_TRIANGLE = Triangle2f(Vector2f.ZERO, Vector2f.X, Vector2f.Y)
        val IDENTITY_RECT = Box2f.ONE
        val IDENTITY_QUAD = IDENTITY_RECT.toQuad()
    }

    fun add(material: Material, vertexColor: ColorFormat? = null, hasOpacity: Boolean = false, geometryBuilder: VertexDataBuilder.() -> Unit)

    fun add(color: Color, vertexColor: ColorFormat? = null, hasOpacity: Boolean = false, geometryBuilder: VertexDataBuilder.() -> Unit) =
        add(SimpleMaterial(color),  vertexColor, hasOpacity, geometryBuilder)


    /**
     * TRIANGLE
     */
    fun addTriangleList(material: Material, triangleList: List<Triangle2f>, matrix: Matrix3f = Matrix3f.IDENTITY) = add(material) {
        if (matrix == Matrix3f.IDENTITY) {
            triangleList.forEach {
                addTriangle2f(it)
            }
        } else {
            triangleList.forEach {
                addTriangle2f(matrix.transform(it))
            }
        }
    }

    fun addTriangleList(color: Color, triangleList: List<Triangle2f>, matrix: Matrix3f = Matrix3f.IDENTITY) = add(color) {
        if (matrix == Matrix3f.IDENTITY) {
            triangleList.forEach {
                addTriangle2f(it)
            }
        } else {
            triangleList.forEach {
                addTriangle2f(matrix.transform(it))
            }
        }
    }

    fun addTriangleContour(
        material: Material,
        triangle: Triangle2f,
        contourThick: Float = 2f,
        mode: BorderMode = BorderMode.CENTER,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) =
        addPolygonContour(material, triangle, contourThick, mode, matrix)

    fun addTriangleContour(
        color: Color,
        triangle: Triangle2f,
        contourThick: Float = 2f,
        mode: BorderMode = BorderMode.CENTER,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) =
        addPolygonContour(color, triangle, contourThick, mode, matrix)

    /**
     * QUAD
     */
    fun addQuad(material: Material, positions: Quad2f, matrix: Matrix3f = Matrix3f.IDENTITY) = add(material) {
        addQuad2f(matrix.transform(positions))
    }

    fun addQuad(material: Material, positions: Quad2f, texturePositions: Quad2f, matrix: Matrix3f = Matrix3f.IDENTITY) = add(material) {
        addQuad2f(matrix.transform(positions), texturePositions)
    }

    fun addQuad(color: Color, positions: Quad2f, matrix: Matrix3f = Matrix3f.IDENTITY) = add(color) {
        addQuad2f(matrix.transform(positions))
    }

    fun addQuad(color: Color, positions: Quad2f, texturePositions: Quad2f, matrix: Matrix3f = Matrix3f.IDENTITY) = add(color) {
        addQuad2f(matrix.transform(positions), texturePositions)
    }

    fun addQuadList(material: Material, quadList: List<Quad2f>, matrix: Matrix3f = Matrix3f.IDENTITY) = add(material) {
        if (matrix == Matrix3f.IDENTITY) {
            quadList.forEach {
                addQuad2f(it)
            }
        } else {
            quadList.forEach {
                addQuad2f(matrix.transform(it))
            }
        }
    }

    fun addQuadList(color: Color, quadList: List<Quad2f>, matrix: Matrix3f = Matrix3f.IDENTITY) = add(color) {
        if (matrix == Matrix3f.IDENTITY) {
            quadList.forEach {
                addQuad2f(it)
            }
        } else {
            quadList.forEach {
                addQuad2f(matrix.transform(it))
            }
        }
    }

    fun addQuadContour(
        material: Material,
        quad: Quad2f,
        contourThick: Float = 2f,
        mode: BorderMode = BorderMode.CENTER,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) =
        addPolygonContour(material, quad, contourThick, mode, matrix)

    fun addQuadContour(
        color: Color,
        quad: Quad2f,
        contourThick: Float = 2f,
        mode: BorderMode = BorderMode.CENTER,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) =
        addPolygonContour(color, quad, contourThick, mode, matrix)

    /**
     * RECT
     */

    fun addRect(material: Material, positions: Box2f, texturePositions: Box2f, matrix: Matrix3f = Matrix3f.IDENTITY) {
        addQuad(material, positions.toQuad(), texturePositions.toQuad(), matrix)
    }


    fun addRect(color: Color, positions: Box2f, matrix: Matrix3f = Matrix3f.IDENTITY) {
        addQuad(color, positions.toQuad(), matrix)
    }

    fun addRect(material: Material, positions: Box2f, matrix: Matrix3f = Matrix3f.IDENTITY) {
        addQuad(material, positions.toQuad(), matrix)
    }

    fun addRect(color: Color, positions: Box2f, texturePositions: Box2f, matrix: Matrix3f = Matrix3f.IDENTITY) {
        addQuad(color, positions.toQuad(), texturePositions.toQuad(), matrix)
    }

    fun addRoundRect(
        material: Material,
        area: Box2f,
        leftTopRange: Float,
        rightTopRange: Float,
        rightBottomRange: Float,
        leftBottomRange: Float,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) {
        addTriangleList(material, Triangulator.roundRect(area, leftTopRange, rightTopRange, rightBottomRange, leftBottomRange), matrix)
    }

    fun addRoundRect(
        color: Color,
        area: Box2f,
        leftTopRange: Float,
        rightTopRange: Float,
        rightBottomRange: Float,
        leftBottomRange: Float,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) {
        addTriangleList(color, Triangulator.roundRect(area, leftTopRange, rightTopRange, rightBottomRange, leftBottomRange), matrix)
    }

    fun addRoundRect(
        material: Material,
        area: Box2f,
        range: Float,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) {
        addRoundRect(material, area, range, range, range, range, matrix)
    }

    fun addRoundRect(
        color: Color,
        area: Box2f,
        range: Float,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) {
        addRoundRect(color, area, range, range, range, range, matrix)
    }


    /**
     * CURVE
     */

    fun addCurve(material: Material, curve: Curve2f, thick: Float = 2f, parts: Int = 128, matrix: Matrix3f = Matrix3f.IDENTITY) {
        addTriangleList(material, Triangulator.curve(curve, thick, parts), matrix)
    }

    fun addCurve(color: Color, curve: Curve2f, thick: Float = 2f, parts: Int = 128, matrix: Matrix3f = Matrix3f.IDENTITY) {
        addTriangleList(color, Triangulator.curve(curve, thick, parts), matrix)
    }


    /**
     * ARROW
     */


    fun addArrow(
        material: Material,
        curve: Curve2f,
        lineThick: Float = 2f,
        arrowMode: ArrowMode = UniformArrowMode(),
        parts: Int = 128,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) {
        addTriangleList(material, Triangulator.arrow(curve, lineThick, arrowMode, parts), matrix)
    }


    fun addArrow(
        color: Color,
        curve: Curve2f,
        lineThick: Float = 2f,
        arrowMode: ArrowMode = UniformArrowMode(),
        parts: Int = 128,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) {
        addTriangleList(color, Triangulator.arrow(curve, lineThick, arrowMode, parts), matrix)
    }

    /**
     * POINT
     */

    fun addPoint(material: Material, center: Vector2f, diameter: Float = 2f, steps: Int = 16, matrix: Matrix3f = Matrix3f.IDENTITY) {
        addTriangleList(material, Triangulator.point(center, diameter, steps), matrix)
    }


    fun addPoint(color: Color, center: Vector2f, diameter: Float = 2f, steps: Int = 16, matrix: Matrix3f = Matrix3f.IDENTITY) {
        addTriangleList(color, Triangulator.point(center, diameter, steps), matrix)
    }


    /**
     * LINE
     */

    fun addLine(material: Material, line: Line2f, thick: Float = 2f, matrix: Matrix3f = Matrix3f.IDENTITY) {
        addTriangleList(material, Triangulator.line(line, thick), matrix)
    }

    fun addLine(color: Color, line: Line2f, thick: Float = 2f, matrix: Matrix3f = Matrix3f.IDENTITY) {
        addTriangleList(color, Triangulator.line(line, thick), matrix)
    }

    fun addLine(material: Material, line: LineSegment2f, thick: Float = 2f, matrix: Matrix3f = Matrix3f.IDENTITY) {
        addTriangleList(material, Triangulator.segment(line, thick), matrix)
    }


    fun addLine(color: Color, line: LineSegment2f, thick: Float = 2f, matrix: Matrix3f = Matrix3f.IDENTITY) {
        addTriangleList(color, Triangulator.segment(line, thick), matrix)
    }

    /**
     * POLYGON
     */

    fun addPolygon(material: Material, polygon: Polygon2f, matrix: Matrix3f = Matrix3f.IDENTITY) {
        addTriangleList(material, Triangulator.polygon(polygon), matrix)
    }


    fun addPolygon(color: Color, polygon: Polygon2f, matrix: Matrix3f = Matrix3f.IDENTITY) {
        addTriangleList(color, Triangulator.polygon(polygon), matrix)
    }

    fun addPolygonContour(
        material: Material,
        polygon: Polygon2f,
        contourThick: Float = 2f,
        mode: BorderMode = BorderMode.CENTER,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) {
        val info = Triangulator.polygonWithContour(polygon, contourThick, mode)
        addTriangleList(material, info.border, matrix)
    }

    fun addPolygonContour(
        color: Color,
        polygon: Polygon2f,
        contourThick: Float = 2f,
        mode: BorderMode = BorderMode.CENTER,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) {
        val info = Triangulator.polygonWithContour(polygon, contourThick, mode)
        addTriangleList(color, info.border, matrix)
    }

    fun addPolygonWithContour(
        main: Material,
        contour: Material,
        polygon: Polygon2f,
        contourThick: Float = 2f,
        mode: BorderMode = BorderMode.CENTER,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) {
        val data = Triangulator.polygonWithContour(polygon, contourThick, mode)

        addTriangleList(main, data.body, matrix)
        addTriangleList(contour, data.border, matrix)
    }

    fun addPolygonWithContour(
        main: Color,
        contour: Color,
        polygon: Polygon2f,
        contourThick: Float = 2f,
        mode: BorderMode = BorderMode.CENTER,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) {
        val data = Triangulator.polygonWithContour(polygon, contourThick, mode)

        addTriangleList(main, data.body, matrix)
        addTriangleList(contour, data.border, matrix)
    }



    /**
     * CIRCLE
     */

    fun addCircle(
        material: Material,
        center: Vector2f,
        rangeOutside: Float,
        rangeInside: Float = 0f,
        steps: Int = 64,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) {
        addTriangleList(material, Triangulator.circle(center, rangeInside, rangeOutside, steps), matrix)
    }


    fun addCircle(
        color: Color,
        center: Vector2f,
        rangeOutside: Float,
        rangeInside: Float = 0f,
        steps: Int = 64,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) {
        addTriangleList(color, Triangulator.circle(center, rangeInside, rangeOutside, steps), matrix)
    }

    /**
     * CIRCLE
     */

    fun addArc(
        material: Material,
        center: Vector2f,
        rangeOutside: Float,
        rangeInside: Float = 0f,
        startAngle: RadianFloat,
        finishAngle: RadianFloat,
        steps: Int = 64,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) {
        addTriangleList(material, Triangulator.arc(center, rangeInside, rangeOutside, steps, startAngle, finishAngle), matrix)
    }

    fun addArc(
        color: Color,
        center: Vector2f,
        rangeOutside: Float,
        rangeInside: Float = 0f,
        startAngle: RadianFloat,
        finishAngle: RadianFloat,
        steps: Int = 64,
        matrix: Matrix3f = Matrix3f.IDENTITY
    ) {
        addTriangleList(color, Triangulator.arc(center, rangeInside, rangeOutside, steps, startAngle, finishAngle), matrix)
    }

}