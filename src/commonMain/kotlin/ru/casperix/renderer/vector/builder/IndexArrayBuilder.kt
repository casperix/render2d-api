package ru.casperix.renderer.vector.builder

import kotlin.math.max
import kotlin.math.roundToInt


class IndexArrayBuilder(initialSize: Int = 16) {
    private val allocateRatio = 2f

    private var indexOffset = -1
    private var indices = UIntArray(initialSize)

    fun getOffset(): Int {
        return indexOffset
    }

    fun clear() {
        indexOffset = -1
    }

    fun next(value: UInt) {
        if (++indexOffset >= indices.size) {
            val next = UIntArray(max(8, (indices.size * allocateRatio).roundToInt()))
            if (indices.size != 0) {
                indices.copyInto(next)
            }
            indices = next
        }

        indices[indexOffset] = value
    }

    fun build(): UIntArray {
        if (indexOffset < 0) return uintArrayOf()
        return indices.sliceArray(0..indexOffset)
    }

    fun addTriangleIndices(v0: UInt, v1: UInt, v2: UInt) {
        next(v0)
        next(v1)
        next(v2)
    }

    fun addQuadIndex(v0: UInt, v1: UInt, v2: UInt, v3: UInt) {
        next(v0)
        next(v1)
        next(v2)
        next(v0)
        next(v2)
        next(v3)
    }

}