package ru.casperix.renderer.vector.builder

import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.renderer.material.Material
import ru.casperix.renderer.vector.VectorGraphic2D
import ru.casperix.renderer.vector.VectorShape2D
import ru.casperix.renderer.vector.vertex.ColorFormat
import ru.casperix.renderer.vector.vertex.VectorFormat

class VectorGraphicBuilder2D : VectorGraphicBuilderExtension2D {
    private val vertexBuilderMap = mutableMapOf<VertexType, VertexDataBuilder>()
    private val additionalShapes = mutableListOf<VectorShape2D>()

    private fun getVertexBuilder(material: Material, vertexColor: ColorFormat? = null, hasOpacity: Boolean = false): VertexDataBuilder {
        val type = VertexType(material, vertexColor, hasOpacity, VectorFormat.VECTOR_2D)
        return vertexBuilderMap.getOrPut(type) {
            val attributes = type.createVertexAttributes()
            VertexDataBuilder(attributes)
        }
    }

    fun add(graphic: VectorGraphic2D, transform:Matrix3f = Matrix3f.IDENTITY) {
        add(graphic.shapes, transform)
    }

    fun add(shapes:List<VectorShape2D>, transform:Matrix3f = Matrix3f.IDENTITY) {
        additionalShapes += if (transform == Matrix3f.IDENTITY) {
            shapes
        } else {
            shapes.map { VectorShape2D(it.material, it.vertexData, it.transform * transform) }
        }
    }

    override fun add(material: Material, vertexColor: ColorFormat?, hasOpacity: Boolean, geometryBuilder: VertexDataBuilder.() -> Unit) {
        getVertexBuilder(material, vertexColor, hasOpacity).build(geometryBuilder)
    }

    fun build(): VectorGraphic2D {
        return VectorGraphic2D(additionalShapes + vertexBuilderMap.map { (type, builder) ->
            val data = builder.build()
            VectorShape2D(type.material, data)
        })
    }

    companion object {
        fun build(builder: VectorGraphicBuilder2D.() -> Unit) =
            VectorGraphicBuilder2D().apply(builder).build()
    }
}