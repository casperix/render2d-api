package ru.casperix.renderer.vector.builder

import ru.casperix.renderer.vector.vertex.VertexArray
import ru.casperix.renderer.vector.vertex.VertexAttributes
import kotlin.math.max
import kotlin.math.roundToInt

class VertexArrayBuilder(attributes: VertexAttributes, initialSize: Int = 16) : VertexBuilder {
    private val allocateRatio = 2f

    private var vertexOffset = -1
    private var vertices = VertexArray(initialSize, attributes)

    fun getOffset(): Int {
        return vertexOffset
    }

    fun clear() {
        vertexOffset = -1
    }

    fun next(): UInt {
        if (++vertexOffset >= vertices.size) {
            val next = VertexArray(max(8, (vertices.size * allocateRatio).roundToInt()), vertices.attributes)
            if (vertices.size != 0) {
                vertices.data.copyInto(next.data)
            }
            vertices = next
        }
        return vertexOffset.toUInt()
    }

    override fun setPosition2(x: Float, y: Float) {
        vertices.setPosition2(vertexOffset, x, y)
    }

    override fun setPosition3(x: Float, y: Float, z: Float) {
        vertices.setPosition3(vertexOffset, x, y, z)
    }

    override fun setNormal2(x: Float, y: Float) {
        vertices.setNormal2(vertexOffset, x, y)
    }

    override fun setNormal3(x: Float, y: Float, z: Float) {
        vertices.setNormal3(vertexOffset, x, y, z)
    }

    override fun setTextureCoord2(u: Float, v: Float) {
        vertices.setTextureCoord2(vertexOffset, u, v)
    }

    override fun setTextureCoord3(u: Float, v: Float, w: Float) {
        vertices.setTextureCoord3(vertexOffset, u, v, w)
    }

    override fun setTangent2(x: Float, y: Float) {
        vertices.setTangent2(vertexOffset, x, y)
    }

    override fun setTangent3(x: Float, y: Float, z: Float) {
        vertices.setTangent3(vertexOffset, x, y, z)
    }

    override fun setRGB(red: Float, green: Float, blue: Float) {
        vertices.setColor(vertexOffset, red, green, blue)
    }

    override fun setOpacity(opacity: Float) {
        vertices.setOpacity(vertexOffset, opacity)
    }


    fun build(): VertexArray = vertices.run {
        if (vertexOffset < 0) return VertexArray(0, attributes, floatArrayOf())
        val vertexAmount = vertexOffset + 1
        val subData = data.sliceArray(0 until (vertexAmount * vertexSize))
        return VertexArray(vertexAmount, attributes, subData)
    }

}