package ru.casperix.renderer.vector.builder

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Color
import ru.casperix.math.curve.float32.Curve2f
import ru.casperix.math.geometry.*
import ru.casperix.math.geometry.builder.ArrowMode
import ru.casperix.math.geometry.builder.BorderMode
import ru.casperix.math.geometry.builder.Triangulator
import ru.casperix.math.geometry.builder.UniformArrowMode
import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.toQuad
import ru.casperix.renderer.vector.vertex.Vertex

interface VertexDataBuilderExtension {
    fun addQuadVertex(quad: Quad<Vertex>)
    fun addTriangleVertex(triangle: Triangle<Vertex>)

    fun addQuad2f(p00: Vector2f, p10: Vector2f, p11: Vector2f, p01: Vector2f, builder: (VertexBuilder.(Vector2f) -> Unit)? = null) {
        addQuad(
            { setPosition2(p00); builder?.invoke(this, p00) },
            { setPosition2(p10); builder?.invoke(this, p10) },
            { setPosition2(p11); builder?.invoke(this, p11) },
            { setPosition2(p01); builder?.invoke(this, p01) },
        )
    }

    fun addQuad3f(p00: Vector3f, p10: Vector3f, p11: Vector3f, p01: Vector3f, builder: (VertexBuilder.(Vector3f) -> Unit)? = null) {
        addQuad(
            { setPosition3(p00); builder?.invoke(this, p00) },
            { setPosition3(p10); builder?.invoke(this, p10) },
            { setPosition3(p11); builder?.invoke(this, p11) },
            { setPosition3(p01); builder?.invoke(this, p01) },
        )
    }

    fun addQuad(builder: VertexBuilder.(Int) -> Unit)
    fun addTriangle(builder: VertexBuilder.(Int) -> Unit)

    fun addTriangle(v0: VertexBuilder.() -> Unit, v1: VertexBuilder.() -> Unit, v2: VertexBuilder.() -> Unit)
    fun addQuad(v0: VertexBuilder.() -> Unit, v1: VertexBuilder.() -> Unit, v2: VertexBuilder.() -> Unit, v3: VertexBuilder.() -> Unit)

    fun addQuad2f(positions: Quad2f) = positions.run {
        addQuad2f(v0, v1, v2, v3)
    }

    fun addQuad2f(
        p00: Vector2f,
        p10: Vector2f,
        p11: Vector2f,
        p01: Vector2f,
        t00: Vector2f,
        t10: Vector2f,
        t11: Vector2f,
        t01: Vector2f,
        generateTangent: Boolean = true
    ) {
        val tangent = if (generateTangent) {
            (p10 - p00).normalize()
        } else null

        addQuad(
            { setPosition2(p00); setTextureCoord2(t00); tangent?.let { setTangent2(it) } },
            { setPosition2(p10); setTextureCoord2(t10); tangent?.let { setTangent2(it) } },
            { setPosition2(p11); setTextureCoord2(t11); tangent?.let { setTangent2(it) } },
            { setPosition2(p01); setTextureCoord2(t01); tangent?.let { setTangent2(it) } },
        )
    }

    fun addQuad2f(p00: Vector2f, p10: Vector2f, p11: Vector2f, p01: Vector2f, color: Color) =
        addQuad2f(p00, p10, p11, p01, color, color, color, color)

    fun addQuad2f(p00: Vector2f, p10: Vector2f, p11: Vector2f, p01: Vector2f, c00: Color, c10: Color, c11: Color, c01: Color) {
        addQuad(
            { setPosition2(p00); setColor(c00) },
            { setPosition2(p10); setColor(c10) },
            { setPosition2(p11); setColor(c11) },
            { setPosition2(p01); setColor(c01) },
        )
    }


    fun addQuad2f(positions: Quad2f, textures: Quad2f, generateTangent: Boolean = true) =
        addQuad2f(positions.v0, positions.v1, positions.v2, positions.v3, textures.v0, textures.v1, textures.v2, textures.v3, generateTangent)

    fun addTriangle2f(positions: Triangle2f) = positions.run {
        addTriangle2f(v0, v1, v2)
    }

    fun addTriangle2f(pos0: Vector2f, pos1: Vector2f, pos2: Vector2f) {
        addTriangle(
            { setPosition2(pos0) },
            { setPosition2(pos1) },
            { setPosition2(pos2) },
        )
    }

    fun addTriangle2f(pos0: Vector2f, pos1: Vector2f, pos2: Vector2f, color: Color) {
        addTriangle(
            { setPosition2(pos0); setColor(color) },
            { setPosition2(pos1); setColor(color) },
            { setPosition2(pos2); setColor(color) },
        )
    }

    fun addTriangle2f(pos0: Vector2f, pos1: Vector2f, pos2: Vector2f, c0: Color, c1: Color, c2: Color) {
        addTriangle(
            { setPosition2(pos0); setColor(c0) },
            { setPosition2(pos1); setColor(c1) },
            { setPosition2(pos2); setColor(c2) },
        )
    }


    fun addTriangle2f(p0: Vector2f, p1: Vector2f, p2: Vector2f, t0: Vector2f, t1: Vector2f, t2: Vector2f, generateTangent: Boolean = true) {
        val tangent = if (generateTangent) {
            (p1 - p0).normalize()
        } else null

        addTriangle(
            { setPosition2(p0); setTextureCoord2(t0); tangent?.let { setTangent2(it) } },
            { setPosition2(p1); setTextureCoord2(t1); tangent?.let { setTangent2(it) } },
            { setPosition2(p2); setTextureCoord2(t2); tangent?.let { setTangent2(it) } },
        )
    }


    fun addTriangle2f(positions: Triangle2f, textures: Triangle2f, generateTangent: Boolean = true) = positions.run {
        addTriangle2f(v0, v1, v2, textures.v0, textures.v1, textures.v2, generateTangent)
    }


    fun addTriangle2f(positions: Triangle2f, color: Color) = positions.run {
        addTriangle2f(v0, v1, v2, color)
    }

    fun addQuad2f(positions: Quad2f, color: Color) = positions.run {
        addQuad2f(v0, v1, v2, v3, color)
    }

    fun addBox2f(positions: Box2f) =
        addQuad2f(positions.toQuad())

    fun addBox2f(positions: Box2f, texturePositions: Box2f) =
        addQuad2f(positions.toQuad(), texturePositions.toQuad())

    fun addRect(positions: Box2f) =
        addBox2f(positions)

    fun addRect(positions: Box2f, texturePositions: Box2f) =
        addBox2f(positions, texturePositions)

    fun addTriangleList2f(triangleList: List<Triangle2f>) =
        triangleList.forEach {
            addTriangle2f(it)
        }

    fun addQuadList2f(quadList: List<Quad2f>) =
        quadList.forEach {
            addQuad2f(it)
        }

    fun addCurve2f(curve: Curve2f, thick: Float = 2f, parts: Int = 128) =
        addTriangleList2f(Triangulator.curve(curve, thick, parts))


    fun addArrow2f(curve: Curve2f, lineThick: Float = 2f, arrowMode: ArrowMode = UniformArrowMode(), parts: Int = 128) =
        addTriangleList2f(Triangulator.arrow(curve, lineThick, arrowMode, parts))

    fun addPoint2f(center: Vector2f, diameter: Float = 2f, steps: Int = 16) =
        addTriangleList2f(Triangulator.point(center, diameter, steps))

    fun addPolygon2f(polygon: Polygon2f) =
        addTriangleList2f(Triangulator.polygon(polygon))

    fun addLine2f(line: Line2f, thick: Float = 2f) =
        addTriangleList2f(Triangulator.line(line, thick))

    fun addLine2f(line: LineSegment2f, thick: Float = 2f) =
        addTriangleList2f(Triangulator.segment(line, thick))

    fun addCircle2f(center: Vector2f, rangeOutside: Float, rangeInside: Float = 0f, steps: Int = 64) =
        addTriangleList2f(Triangulator.circle(center, rangeInside, rangeOutside, steps))

    fun addArc2f(
        center: Vector2f,
        rangeOutside: Float,
        rangeInside: Float = 0f,
        startAngle: RadianFloat,
        finishAngle: RadianFloat,
        steps: Int = 64
    ) =
        addTriangleList2f(Triangulator.arc(center, rangeInside, rangeOutside, steps, startAngle, finishAngle))

    fun addArc2f(
        center: Vector2f,
        rangeOutside: Float,
        rangeInside: Float = 0f,
        startAngle: DegreeFloat,
        finishAngle: DegreeFloat,
        steps: Int = 64
    ) =
        addArc2f(center, rangeOutside, rangeInside, startAngle.toRadian(), finishAngle.toRadian(), steps)


    fun addPolygonContour(polygon: Polygon2f, contourThick: Float = 2f, mode: BorderMode = BorderMode.CENTER) =
        Triangulator.polygonWithContour(polygon, contourThick, mode)

    fun addQuadContour(quad: Quad2f, contourThick: Float = 2f, mode: BorderMode = BorderMode.CENTER) =
        addPolygonContour(quad, contourThick, mode)


    fun addTriangleContour(triangle: Triangle2f, contourThick: Float = 2f, mode: BorderMode = BorderMode.CENTER) =
        addPolygonContour(triangle, contourThick, mode)


    fun addRoundRect(
        area: Box2f,
        leftTopRange: Float,
        rightTopRange: Float,
        rightBottomRange: Float,
        leftBottomRange: Float
    ) =
        addTriangleList2f(Triangulator.roundRect(area, leftTopRange, rightTopRange, rightBottomRange, leftBottomRange))
}