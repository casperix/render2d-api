package ru.casperix.renderer

import ru.casperix.math.color.Color
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder2D
import kotlin.test.Test

class VectorGraphicBuilderTest {
    @Test
    fun simple() {
        VectorGraphicBuilder2D.build {
            add(Color.RED) {
                addCircle(Vector2f.ZERO, 4f, 3f)
            }

            addCircle(Color.GREEN, Vector2f.ZERO, 2f)
        }
    }
}