package ru.casperix.renderer.vertex

import ru.casperix.renderer.vector.vertex.ColorFormat
import ru.casperix.renderer.vector.vertex.VertexArray
import kotlin.test.Test
import kotlin.test.assertEquals

class VertexArrayTest {
    @Test
    fun test() {
        val bools = listOf(true, false)


        bools.forEach { usePosition2 ->
            bools.forEach { useTextureCoord ->
                bools.forEach { useTangent ->
                    bools.forEach { usePosition3 ->
                        bools.forEach { useColor ->
                            val buffer = VertexArray(3, usePosition2, useTextureCoord, useTangent, usePosition3, if (useColor) ColorFormat.RGB else null)

                            var c = 0f
                            (0..2).forEach { v ->
                                if (usePosition2) buffer.setPosition2(v, c++, c++)
                                if (useTextureCoord) buffer.setTextureCoord2(v, c++, c++)
                                if (useTangent) buffer.setTangent2(v, c++, c++)
                                if (usePosition3) buffer.setPosition3(v, c++, c++, c++)
                                if (useColor) buffer.setColor(v, c++, c++, c++)
                            }


                            buffer.data.forEachIndexed { index, fl ->
                                assertEquals(index.toFloat(), fl)
                            }
                        }
                    }

                }
            }
        }

    }
}