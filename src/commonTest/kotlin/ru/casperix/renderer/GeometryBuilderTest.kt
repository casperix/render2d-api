package ru.casperix.renderer

import ru.casperix.renderer.vector.builder.VertexDataBuilder.Companion.generateQuadIndices
import ru.casperix.renderer.vector.builder.VertexDataBuilder.Companion.generateTriangleIndices
import kotlin.test.Test
import kotlin.test.assertContentEquals

class GeometryBuilderTest {
    @Test
    fun triangleIndexTest() {
        val generated = generateTriangleIndices(2)
        assertContentEquals(uintArrayOf(0u, 1u, 2u, 3u, 4u, 5u), generated)
    }

    @Test
    fun quadIndexTest() {
        val generated = generateQuadIndices(2)
        assertContentEquals(uintArrayOf(0u, 1u, 2u, 0u, 2u, 3u, 0u + 4u, 1u + 4u, 2u + 4u, 0u + 4u, 2u + 4u, 3u + 4u), generated)
    }
}