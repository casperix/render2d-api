package ru.casperix.renderer.builder

import ru.casperix.math.geometry.Quad2f
import ru.casperix.math.geometry.Triangle2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.vector.builder.IndexArrayBuilder
import ru.casperix.renderer.vector.builder.VertexDataBuilder
import ru.casperix.renderer.vector.vertex.VertexAttributes
import ru.casperix.renderer.vector.builder.VertexArrayBuilder
import ru.casperix.renderer.vector.vertex.ColorFormat
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue


class VectorBuilderTest {
    @Test
    fun emptyIndexBuilder() {
        val b0 = IndexArrayBuilder(0)
        val r0 = b0.build()
        assertTrue(r0.isEmpty())

        val b1 = IndexArrayBuilder(0)
        b1.next(1u)
        val r1 = b1.build()
        assertTrue(r1.size == 1)
        assertTrue(r1[0] == 1u)
    }

    @Test
    fun emptyVertexBuilder() {
        val b0 = VertexArrayBuilder(VertexAttributes(), 0)
        val r0 = b0.build()
        assertTrue(r0.size == 0)
    }

    @Test
    fun indexBuilder() {
        val size = 1000

        val builder = IndexArrayBuilder(size)

        repeat(size) {
            builder.next(it.toUInt())
        }

        val array = builder.build()
        assertEquals(1000, array.size)

        repeat(size) {
            assertEquals(it.toUInt(), array[it])
        }

    }

    @Test
    fun vertexPositionOnlyBuilder() {
        val size = 1000

        val builder = VertexArrayBuilder(VertexAttributes(hasPosition2 = true), size)

        repeat(size) {
            builder.next()

            val tx = it.toFloat()
            val ty = it.toFloat() * 2f
            builder.setPosition2(tx, ty)
        }

        val array = builder.build()
        assertEquals(1000, array.size)

        repeat(size) {
            val tx = it.toFloat()
            val ty = it.toFloat() * 2f

            val ax = array.data[it * array.vertexSize + array.position2Offset + 0]
            val ay = array.data[it * array.vertexSize + array.position2Offset + 1]

            assertEquals(tx, ax)
            assertEquals(ty, ay)
        }

    }

    @Test
    fun vertexFullBuilder() {
        val size = 1000

        val builder = VertexArrayBuilder(VertexAttributes(true, true, true, true, ColorFormat.RGB), size)

        repeat(size) {
            builder.next()

            var s = it.toFloat()

            builder.setPosition2(s++, s++)
            builder.setTextureCoord(s++, s++)
            builder.setTangent(s++, s++)
            builder.setPosition3(s++, s++, s++)
            builder.setRGB(s++, s++, s++)
        }

        val array = builder.build()
        assertEquals(1000, array.size)

        repeat(size) {
            var s = it.toFloat()

            val tList = array.run {
                listOf(
                    data[it * vertexSize + position2Offset + 0],
                    data[it * vertexSize + position2Offset + 1],

                    data[it * vertexSize + textureCoordOffset + 0],
                    data[it * vertexSize + textureCoordOffset + 1],

                    data[it * vertexSize + tangentOffset + 0],
                    data[it * vertexSize + tangentOffset + 1],

                    data[it * vertexSize + position3Offset + 0],
                    data[it * vertexSize + position3Offset + 1],
                    data[it * vertexSize + position3Offset + 2],

                    data[it * vertexSize + colorOffset + 0],
                    data[it * vertexSize + colorOffset + 1],
                    data[it * vertexSize + colorOffset + 2],
                    data[it * vertexSize + colorOffset + 3],
                )
            }

            tList.forEach { t ->
                assertEquals(s++, t)
            }
        }
    }

    @Test
    fun geometryBuilder() {
        val builder = VertexDataBuilder(VertexAttributes(hasPosition2 = true))

        var s = 0f
        builder.addQuad2f(Quad2f(Vector2f(s++, s++), Vector2f(s++, s++), Vector2f(s++, s++), Vector2f(s++, s++)))
        builder.addQuad2f(Quad2f(Vector2f(s++, s++), Vector2f(s++, s++), Vector2f(s++, s++), Vector2f(s++, s++)))
        builder.addTriangle2f(Triangle2f(Vector2f(s++, s++), Vector2f(s++, s++), Vector2f(s++, s++)))

        val data = builder.build()

        val expected = uintArrayOf(0u, 1u, 2u, 0u, 2u, 3u, 0u + 4u, 1u + 4u, 2u + 4u, 0u + 4u, 2u + 4u, 3u + 4u, 8u, 9u, 10u)
        assertTrue(expected.contentEquals(data.indices))

        val vertices = data.vertices.data
        assertEquals(vertices.size, 22)
        vertices.forEachIndexed { index, value ->
            assertEquals(index.toFloat(), value)
        }
    }
}