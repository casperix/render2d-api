package ru.casperix.renderer

import ru.casperix.math.array.uint8.UByteArray3D
import ru.casperix.math.color.Color
import ru.casperix.renderer.pixel_map.Float32PixelMap
import ru.casperix.renderer.pixel_map.Rgb32PixelMap
import ru.casperix.renderer.pixel_map.Rgb8PixelMap
import ru.casperix.renderer.pixel_map.Rgba8PixelMap
import kotlin.test.Test
import kotlin.test.assertEquals


class PixelMapTest {
    @Test
    fun create() {
        listOf(
            Rgb8PixelMap(3, 1),
            Rgb8PixelMap(UByteArray3D(3, 1, 3)),
        ).forEach { map ->
            assertEquals(3, map.width)
            assertEquals(1, map.height)
            assertEquals(9, map.byteArray.size)
        }

        listOf(
            Rgba8PixelMap(3, 1),
            Rgba8PixelMap(UByteArray3D(3, 1, 4)),
        ).forEach { map ->
            assertEquals(3, map.width)
            assertEquals(1, map.height)
            assertEquals(12, map.byteArray.size)
        }

        listOf(
            Float32PixelMap(3, 1),
            Float32PixelMap(UByteArray3D(3, 1, 4)),
        ).forEach { map ->
            assertEquals(3, map.width)
            assertEquals(1, map.height)
            assertEquals(12, map.byteArray.size)
        }


        listOf(
            Rgb32PixelMap(3, 1),
            Rgb32PixelMap(UByteArray3D(3, 1, 12)),
        ).forEach { map ->
            assertEquals(3, map.width)
            assertEquals(1, map.height)
            assertEquals(36, map.byteArray.size)
        }
    }


    @Test
    fun getAndPutRGB() {
        val rgb = Rgb8PixelMap(3, 1)

        val c1 = Color.RED.toColor3b()
        val c2 = Color.GREEN.toRGBA()
        val c3 = Color.BLUE

        rgb.set(0, 0, c1)
        rgb.set(1, 0, c2)
        rgb.set(2, 0, c3)

        assertEquals(c1.toColor3b(), rgb.get(0, 0))
        assertEquals(c2.toRGB().toColor3b(), rgb.get(1, 0))
        assertEquals(c3.toColor3b(), rgb.get(2, 0))

    }

    @Test
    fun getAndPutRGBA() {
        val rgba = Rgba8PixelMap(3, 1)

        val c1 = Color.RED.toRGBA(0f)
        val c2 = Color.GREEN.toRGBA(0.5f)
        val c3 = Color.BLUE.toRGBA(1f)

        rgba.set(0, 0, c1)
        rgba.set(1, 0, c2)
        rgba.set(2, 0, c3)

        assertEquals(c1.toColor4b(), rgba.get(0, 0))
        assertEquals(c2.toColor4b(), rgba.get(1, 0))
        assertEquals(c3.toColor4b(), rgba.get(2, 0))
    }
}